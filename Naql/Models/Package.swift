//
//  Package.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/21/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class Package: Mappable {
    var id: Int = 0
    var name: String = ""
    var description: String = ""
    var shipments: Int = 0
    var price: Double = 0.0
    var createdAt: Date = Date()
    var updatedAt: Date = Date()

    required init?(map: Map) {}

    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        shipments <- (map["number_of_chipment"], TransfromString.ToInt())
        price <- (map["price"], TransfromString.ToDouble())
        createdAt <- (map["created_at"], TransfromString.ToDate())
        updatedAt <- (map["updated_at"], TransfromString.ToDate())
    }
}
