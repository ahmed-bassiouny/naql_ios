//
//  Coordinates.swift
//  Naql
//
//  Created by Mostafa Saleh on 12/6/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import CoreLocation
import Foundation

struct Coordinates {
    var latitude: Double = 0.0
    var longitude: Double = 0.0

    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }

    init(clCoordinates: CLLocationCoordinate2D) {
        latitude = clCoordinates.latitude
        longitude = clCoordinates.longitude
    }
}
