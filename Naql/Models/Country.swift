//
//  Country.swift
//  Naql
//
//  Created by Admin on 1/16/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import Foundation
import ObjectMapper

class Country: Area {
    
    var cities: [City] = []
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override init() {super.init()}
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        cities <- map["cities"]
    }
}
