//
//  Driver.swift
//  Naql
//
//  Created by Mostafa Saleh on 12/6/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class Driver: Mappable, Equatable {
    var id: Int = 0
    var driverId: Int = 0
    var carId:String = ""
    var ownerID: String = ""
    var name: String = ""
    var phone: String = ""
    var plateNumber: String = ""
    var carType: String = ""
    //var formNumber: String = ""
    var color: String = ""
    var model: String = ""
    //var formEndDate: String = ""
    var active = true
    

    init() {}

    required init?(map: Map) {}

    func mapping(map: Map) {
        id <- map["id"]
        driverId <- map["driver_id"]
        carId <- map["car_id"]
        ownerID <- map["owner_id"]
        name <- map["name"]
        phone <- map["phone"]
        plateNumber <- map["plate_number"]
        carType <- map["car_type"]
        //formNumber <- map["form_number"]
        color <- map["color"]
        model <- map["model"]
        //formEndDate <- map["form_end_date"]
        active <- (map["is_active"],TransformInt.ToBool())
    }

    static func == (rhs: Driver, lhs: Driver) -> Bool {
        return rhs.id == lhs.id
    }

    func clone() -> Driver {
        return Mapper<Driver>().map(JSON: toJSON())!
    }
}
