//
//  Area.swift
//  Naql
//
//  Created by Admin on 1/16/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import Foundation
import ObjectMapper

class Area: Mappable  {
    var id: Int = 0
    var nameEn: String = ""
    var nameAr: String = ""
    
    required init?(map: Map) {}
    
    init() {}
    
    func mapping(map: Map) {
        id <- map["id"]
        nameEn <- map["name_en"]
        nameAr <- map["name_ar"]
    }
    
    func getName(showAra:Bool) -> String {
        return showAra ? nameAr : nameEn
    }
}
