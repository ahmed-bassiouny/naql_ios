//
//  Order.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/21/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class Order: Mappable, Equatable {
    var id: Int = 0
    var country: String = ""
    var fromCity: String = ""
    var toCity: String = ""
    var region: String = ""
    var companyName: String = ""
    var orderType: Int = 0
    var tripType: Int = 0 // 0 for one way 1 for round trip
    var numberOfShipmentsAvailable: Int = 0
    var loadInTon: String = ""
    var fromLng: Double = 0.0
    var fromLat: Double = 0.0
    var fromAddress: String = ""
    var toLng: Double = 0.0
    var toLat: Double = 0.0
    var toAddress: String = ""
    var price: String = ""
    var status: OrderStatus = .running
    var points: Int = 0
    var companies: [DriversCompany] = []
    var drivers: [Driver] = []
    var fromTime: Int = -1
    var toTime: Int = -1
    var holiday: String = ""
    var numberOfDrivers: Int = 0
    var createdAt: Date = Date()
    var updatedAt: Date = Date()
    var returnCity: String = ""
    var returnLat: Double = 0.0
    var returnLng: Double = 0.0
    var returnAddress: String = ""
    var saveCost:Int = -1
    var topic:Int = 0
    var requiresSeaport:String = ""
    var typeOfLoad:String = ""
    var descOfLoad:String = ""
    var paymentMethod:Int = 0

    required init?(map: Map) {}

    init() {}

    func mapping(map: Map) {
        id <- map["id"]
        country <- map["country"]
        fromCity <- map["from_city"]
        toCity <- map["to_city"]
        region <- map["region"]
        companyName <- map["company_name"]
        orderType <- (map["order_type"],TransfromString.ToInt())
        tripType <- (map["trip_type"],TransfromString.ToInt())
        numberOfShipmentsAvailable <- map["number_of_shipments_available"]
        loadInTon <- map["Load_in_ton"]
        fromLng <- (map["from_lng"],TransfromString.ToDouble())
        fromLat <- (map["from_lat"],TransfromString.ToDouble())
        fromAddress <- map["from_address"]
        toLng <- (map["to_lng"],TransfromString.ToDouble())
        toLat <- (map["to_lat"],TransfromString.ToDouble())
        toAddress <- map["to_address"]
        price <- map["price"]
        status <- (map["status"], TransfromString.ToOrderStatus())
        points <- (map["points"], TransfromString.ToInt())
        companies <- map["companies"]
        drivers <- map["drivers"]
        fromTime <- (map["from_time"], TransfromString.ToInt())
        toTime <- (map["to_time"], TransfromString.ToInt())
        holiday <- map["holiday"]
        numberOfDrivers <- map["drivers_count"]
        createdAt <- (map["created_at"], TransfromString.ToDate())
        updatedAt <- (map["updated_at"], TransfromString.ToDate())
        returnCity <- map["return_city"]
        returnLat <- (map["return_lat"],TransfromString.ToDouble())
        returnLng <- (map["return_lng"],TransfromString.ToDouble())
        returnAddress <- map["return_address"]
        saveCost <- (map["save_cost"], TransfromString.ToInt())
        topic <- map["topic"]
        requiresSeaport <- map["requires_seaport"]
        paymentMethod <- map["payment_method"]
    }

    static func == (lhs: Order, rhs: Order) -> Bool {
        return lhs.id == rhs.id
    }
}
