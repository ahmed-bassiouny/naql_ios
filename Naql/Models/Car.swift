//
//  Car.swift
//  Naql
//
//  Created by Admin on 1/22/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class Car: Mappable, Equatable {
    var id: Int = 0
    var carId: Int = 0
    var userId = ""
    var plateNumber: String = ""
    var carType: String = ""
    //var formNumber: String = ""
    var color: String = ""
    var model: String = ""
    //var formEndDate: String = ""
    var deleted: Bool = false
    
    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        carId <- map["car_id"]
        userId <- map["user_id"]
        plateNumber <- map["plate_number"]
        carType <- map["car_type"]
        //formNumber <- map["form_number"]
        color <- map["color"]
        model <- map["model"]
        //formEndDate <- map["form_end_date"]
        deleted <- (map["is_deleted"], TransformInt.ToBool())
    }
    
    static func == (rhs: Car, lhs: Car) -> Bool {
        return rhs.id == lhs.id
    }
    
    func clone() -> Car {
        return Mapper<Car>().map(JSON: toJSON())!
    }
}
