//
//  DriversCompany.swift
//  Naql
//
//  Created by Mostafa Saleh on 12/6/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class DriversCompany: Mappable {
    var companyName: String = ""
    var drivers: [Driver] = []
    var numberOfShipments: Int = 0

    init() {}

    required init?(map: Map) {}

    func mapping(map: Map) {
        companyName <- map["name"]
        drivers <- map["drivers"]
        numberOfShipments <- map["companyShipment"]
    }
}
