//
//  User.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/19/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class User: Mappable {
    var id: Int = 0
    var email: String = ""
    var phone: String = ""
    var country: String = ""
    var city: String = ""
    var companyName: String = ""
    var companyActivity: String?
    var commercialNumber: String = ""
    var commercialExpirationDate: String = ""
    var type: UserType = .driver
    var points: Int = 0
    var notificationToken: String = ""
    var licenseName: String?
    var licenseNumber: String?
    var licenseExpirationDate: String?
    var isAdmin: Bool = false
    var isApproved: Bool = false
    var isBlocked: Bool = false
    var createdAt: Date = Date()
    var updatedAt: Date = Date()
    var languageArabic = false
    var taxNumber: String = ""

    private var name: String = "name"

    required init?(map: Map) {}

    init() {}

    func mapping(map: Map) {
        id <- map["id"]
        email <- map["email"]
        phone <- map["phone"]
        country <- map["country"]
        city <- map["city"]
        companyName <- map["company_name"]
        companyActivity <- map["company_activity"]
        commercialNumber <- map["commercial_number"]
        commercialExpirationDate <- map["commercial_date"]
        type <- (map["type"], TransfromString.ToUserType())
        points <- (map["points"], TransfromString.ToInt())
        notificationToken <- map["notification_token"]
        licenseName <- map["license_name"]
        licenseNumber <- map["license_number"]
        licenseExpirationDate <- map["license_date"]
        isAdmin <- (map["is_admin"], TransformInt.ToBool())
        isApproved <- (map["is_approved"], TransformInt.ToBool())
        isBlocked <- (map["is_blocked"], TransformInt.ToBool())
        createdAt <- (map["created_at"], TransfromString.ToDate())
        updatedAt <- (map["updated_at"], TransfromString.ToDate())
        name <- map["name"]
        taxNumber <- map["tax_number"]
    }
}
