//
//  Complaint.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/24/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class Complaint: Mappable {
    var id: Int = 0
    var userID: String = ""
    var name: String = ""
    var email: String = ""
    var phone: String = ""
    var title: String = ""
    var compliant: String = ""
    var createdAt: Date = Date()
    var updatedAt: Date = Date()

    required init?(map: Map) {}

    init() {}

    func mapping(map: Map) {
        id <- map["id"]
        userID <- map["user_id"]
        email <- map["email"]
        phone <- map["phone"]
        title <- map["title"]
        compliant <- map["compliant"]
        createdAt <- (map["created_at"], TransfromString.ToDate())
        updatedAt <- (map["updated_at"], TransfromString.ToDate())
    }
}
