//
//  LocationType.swift
//  Naql
//
//  Created by Admin on 1/7/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import Foundation

enum LocationType {
    case Origin
    case Destination
    case Return
}
