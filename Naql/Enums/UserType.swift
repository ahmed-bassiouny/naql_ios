//
//  UserType.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/19/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

enum UserType {
    case factory
    case driver
}
