//
// Created by Mostafa Saleh on 10/24/18.
// Copyright (c) 2018 Mostafa Saleh. All rights reserved.
//

enum Page {
    case about

    var stringValue: String {
        switch self {
        case .about:
            return "about"
        }
    }
}
