//
//  OrderTableCell.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/21/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

class OrderTableCell: UITableViewCell {
    @IBOutlet var actualContentView: UIView!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var originLabel: UILabel!
    @IBOutlet var originAddressLabel: UILabel!
    @IBOutlet var destinationLabel: UILabel!
    @IBOutlet var destinationAddressLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet weak var shipmentLabel: UILabel!
    
    private let dateFormatter = DateFormatter(withFormat: "dd/MM/yyyy", locale: Locale.current.identifier)

    var order: Order? {
        didSet {
            if let order = order {
                typeLabel.text = String(format: "label_order_type".localized, Constants.orderTypes[order.orderType].localized)
                dateLabel.text = dateFormatter.string(from: order.createdAt)
                originLabel.text = String(format: "label_origin_city".localized, order.fromCity)
                originAddressLabel.text = order.fromAddress
                destinationLabel.text = String(format: "label_destination_city".localized, order.toCity)
                destinationAddressLabel.text = order.toAddress
                priceLabel.text = String(format: "label_proposed_price".localized, order.price)
                statusLabel.text = (order.status == .running ? "label_active" : "label_inactive").localized
                statusLabel.backgroundColor = order.status == .running ? .greenDark : .red
                isLoading = false
                shipmentLabel.text = "\("avaliableshipments".localized) : \(order.numberOfShipmentsAvailable - order.numberOfDrivers)"
            } else {
                isLoading = true
            }
        }
    }

    var isLoading: Bool = true {
        didSet {
            actualContentView.isHidden = isLoading
            /*if isLoading {
                activityIndicator.startAnimating()
            } else {
                activityIndicator.stopAnimating()
            }*/
        }
    }
}
