//
//  MiniDriverTableViewCell.swift
//  Naql
//
//  Created by Admin on 3/7/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import UIKit

class MiniDriverTableViewCell: UITableViewCell {
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var phoneNumber: UILabel!
    
    var driver: Driver? {
        didSet {
            if let driver = driver {
                nameLabel.text = String(format: "label_driver_name".localized, driver.name)
                phoneNumber.text = String(format: "label_phone_number".localized, driver.phone)
            }
        }
    }

}
