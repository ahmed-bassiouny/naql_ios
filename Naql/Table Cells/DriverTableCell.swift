//
//  DriverTableCell.swift
//  Naql
//
//  Created by Mostafa Saleh on 12/6/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

class DriverTableCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var phoneNumber: UILabel!
    @IBOutlet var carType: UILabel!
    @IBOutlet var model: UILabel!
    @IBOutlet var plateNumber: UILabel!
    @IBOutlet var color: UILabel!
   // @IBOutlet var formNumber: UILabel!
   // @IBOutlet var formDate: UILabel!

    var driver: Driver? {
        didSet {
            if let driver = driver {
                nameLabel.text = String(format: "label_driver_name".localized, driver.name)
                phoneNumber.text = String(format: "label_phone_number".localized, driver.phone)
                carType.text = String(format: "car_type".localized, driver.carType)
                model.text = String(format: "model".localized, driver.model)
                plateNumber.text = String(format: "plate_number".localized, driver.plateNumber)
                color.text = String(format: "color".localized, driver.color)
                //formNumber.text = String(format: "form_number".localized, driver.formNumber)
                //formDate.text = String(format: "form_date".localized, driver.formEndDate)
            }
        }
    }
}
