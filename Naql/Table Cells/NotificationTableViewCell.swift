//
//  NotificationTableViewCell.swift
//  Naql
//
//  Created by Admin on 1/11/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusSwitch: UISwitch!
    var delegate : NotificationDelegate?
    var cityId = 0
    var index = 0;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(index:Int,city:City,status:Bool) {
        self.nameLabel.text = city.getName(showAra: Constants.showArabic)
        self.cityId = city.id;
        self.statusSwitch.isOn = status
        self.index = index
    }
    
    @IBAction func switchAction(_ sender: UISwitch) {
        if sender.isOn {
            delegate?.add(cityId,index)
        }else {
            delegate?.remove(cityId,index)
        }
    }
    
}



protocol NotificationDelegate {
    func add(_ id:Int,_ index:Int)
    func remove(_ id:Int,_ index:Int)
}
