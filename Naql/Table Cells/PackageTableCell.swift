//
//  PackageTableCell.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/21/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

class PackageTableCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var numberOfTripsLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!

    var package: Package? {
        didSet {
            if let package = package {
                nameLabel.text = package.name
                descriptionLabel.text = package.description
                numberOfTripsLabel.text = String(format: "%i", package.shipments)
                priceLabel.text =
                    String(format: "%.2f", package.price)
            }
        }
    }
}
