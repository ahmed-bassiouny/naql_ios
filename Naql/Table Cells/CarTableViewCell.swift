//
//  CarTableViewCell.swift
//  Naql
//
//  Created by NTAM on 1/23/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import UIKit

class CarTableViewCell: UITableViewCell {

    @IBOutlet weak var carType: UILabel!
    @IBOutlet weak var model: UILabel!
    @IBOutlet weak var plateNumber: UILabel!
    @IBOutlet weak var color: UILabel!
    //@IBOutlet weak var formNumber: UILabel!
    //@IBOutlet weak var formDate: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    
    }

    var carItem: Car? {
        didSet {
            if let car = carItem {
                carType.text = String(format: "car_type".localized, car.carType)
                model.text = String(format: "model".localized, car.model)
                plateNumber.text = String(format: "plate_number".localized, car.plateNumber)
                color.text = String(format: "color".localized, car.color)
                //formNumber.text = String(format: "form_number".localized, car.formNumber)
                //formDate.text = String(format: "form_date".localized, car.formEndDate)

            }
        }
    }
}
