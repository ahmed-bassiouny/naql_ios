//
//  ComplaintTableCell.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/24/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

class ComplaintTableCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var complaintLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!

    private let dateFormatter = DateFormatter(withFormat: "dd/MM/yyyy", locale: Locale.current.identifier)

    var complaint: Complaint? {
        didSet {
            if let complaint = complaint {
                titleLabel.text = complaint.title
                complaintLabel.text = complaint.compliant
                dateLabel.text = dateFormatter.string(from: complaint.createdAt)
            }
        }
    }
}
