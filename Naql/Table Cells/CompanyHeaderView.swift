//
//  CompanyHeaderView.swift
//  Naql
//
//  Created by Mostafa Saleh on 12/6/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

class CompanyHeaderView: UITableViewHeaderFooterView {
    @IBOutlet var companyNameLabel: UILabel!
    @IBOutlet var numberOfShipmentsLabel: UILabel!
}
