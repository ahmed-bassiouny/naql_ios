//
//  MoreTableViewCell.swift
//  Naql
//
//  Created by Admin on 2/8/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import UIKit

class MoreTableViewCell: UITableViewCell {
    
    @IBOutlet var name: UILabel!
    @IBOutlet var icon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(name:String,icon:UIImage){
        self.name.text = name
        self.icon.image = icon
    }
}
