//
//  AboutUsViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/24/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit
import WebKit

class AboutUsViewController: UIViewController, WKNavigationDelegate {
    @IBOutlet var webView: WKWebView!
    @IBOutlet var activtyIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        ApiController.shared.fetchPage(.about) { result in
            switch result {
            case let .success(html):
                self.load(html: html)
            case let .failure(error):
                AlertUtils.showAlert(parent: self, message: error)
            }
        }
    }

    func load(html: String) {
        startLoading()
        webView.loadHTMLString(html, baseURL: nil)
    }

    func startLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        activtyIndicator.startAnimating()
        activtyIndicator.isHidden = false
        webView.isInvisible = true
    }

    func stopLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        activtyIndicator.stopAnimating()
        webView.isInvisible = false
    }

    // MARK: WKNavigation Delegate

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        stopLoading()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        stopLoading()
    }
}
