//
//  AddDriverViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 12/6/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import MaterialComponents
import UIKit

class AddDriverViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    
    @IBOutlet var orderIdLabel: UILabel!
    @IBOutlet var DriverInfoTextField: MDCTextField!
    @IBOutlet weak var carInfoTextField: MDCTextField!
    @IBOutlet var allowedDriversLabel: UILabel!
    @IBOutlet var driversTableView: UITableView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var saveButton: UIButton!
    
    var order: Order!
    var pickerView: UIPickerView!
    
    private var drivers: [Driver] = [] // selected drivers
    private var selectedCar = Car()
    private var selectedDriver = Driver()
    
    private var cars : [Car] = []
    private var allDrivers : [Driver] = [] // get from requests
    var orderDetailsViewController: OrderDetailsViewController?
    
    
    private var numberOfAllowedDrivers: Int {
        return order.numberOfShipmentsAvailable - order.numberOfDrivers - drivers.count
    }
    private var currentDriver: Driver {
        let driver = Driver()
        driver.name = selectedDriver.name
        driver.phone = selectedDriver.phone
        driver.carId = String(selectedCar.id)
        driver.plateNumber = selectedCar.plateNumber
        driver.carType = selectedCar.carType
        //driver.formNumber = selectedCar.formNumber
        driver.color = selectedCar.color
        driver.model = selectedCar.model
        //driver.formEndDate = selectedCar.formEndDate
        return driver
    }
    
    private var textFieldsControllers: [MDCTextInputControllerOutlined] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextFields()
        fillInfo()
    }
    
    // MARK: Private functions
    
    private func setupTextFields() {
        addController(to: DriverInfoTextField)
        addController(to: carInfoTextField)
        pickerView = UIPickerView()
        
        pickerView.dataSource = self
        pickerView.delegate = self
        
        carInfoTextField.inputView = pickerView
        DriverInfoTextField.inputView = pickerView
        fetchCars()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if DriverInfoTextField.isFirstResponder {
            return allDrivers.count
        }
        return cars.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if DriverInfoTextField.isFirstResponder {
            return allDrivers[row].name
        }
        return "\(cars[row].carType) (\(cars[row].model))"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if DriverInfoTextField.isFirstResponder {
            DriverInfoTextField.text = allDrivers[row].name
            selectedDriver = allDrivers[row]
            self.view.endEditing(true)
            return
        }
        carInfoTextField.text = "\(cars[row].carType) (\(cars[row].model))"
        selectedCar = cars[row]
        self.view.endEditing(true)
    }
    
    private func fillInfo() {
        orderIdLabel.text = String(format: "label_order_id".localized, order.id)
        allowedDriversLabel.text = String(format: "label_allowed_driver_count".localized, numberOfAllowedDrivers)
        saveButton.isHidden = drivers.count == 0
    }
    
    private func addController(to textField: MDCTextField) {
        let textFieldController = MDCTextInputControllerOutlined(textInput: textField)
        textFieldController.floatingPlaceholderActiveColor = .colorAccent
        textFieldController.activeColor = .colorAccent
        textFieldsControllers.append(textFieldController)
    }
    
    private func validate() -> String? {
        if selectedDriver.id < 1 {
            return "select_driver".localized
        } else if selectedCar.id < 1 {
            return "select_car".localized
        }
        return nil
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { _, indexPath in
            self.drivers.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            self.fillInfo()
        }
        return [delete]
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return drivers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DriverTableCell")!
        cell.textLabel?.text = drivers[indexPath.row].name
        return cell
    }
    
    // MARK: IBActions
    
    @IBAction func didClickAddDriver(_ sender: UIButton) {
        view.endEditing(true)
        if let errorMessage = validate() {
            ProgressHUD.shared.show(.error, in: view, withMessage: errorMessage)
            ProgressHUD.shared.dismiss(afterDelay: 1.0)
        } else {
            // remove selected driver from driver list
            if let index = allDrivers.firstIndex(of: selectedDriver) {
                allDrivers.remove(at: index)
            }
            drivers.append(currentDriver)
            driversTableView.insertRows(at: [IndexPath(row: drivers.count - 1, section: 0)], with: .automatic)
            DriverInfoTextField.text = nil
            carInfoTextField.text = nil
            /*carTypeTextField.text = nil*/
            fillInfo()
        }
    }
    
    @IBAction func didClickSave(_ sender: UIButton) {
        view.endEditing(true)
        ProgressHUD.shared.show(.pleaseWait, in: view)
        ApiController.shared.addDrivers(to: order.id, drivers: drivers) { result in
            switch result {
            case .success:
                ProgressHUD.shared.show(.success, in: self.view, withMessage: "message_drivers_success".localized)
                self.orderDetailsViewController?.addedDriversToTrip(numberOfDrivers: self.drivers.count)
                ProgressHUD.shared.dismiss(afterDelay: 1.0) {
                    self.navigationController?.popViewController(animated: true)
                }
            case let .failure(error):
                ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                ProgressHUD.shared.dismiss(afterDelay: 2.0)
            }
        }
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if textField.returnKeyType == .next, let nextResponder = view.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    private func fetchCars() {
        ProgressHUD.shared.show(.pleaseWait, in: view)
        ApiController.shared.fetchCars { result in
            switch result {
            case let .success(cars):
                self.cars = cars
                self.fetchDrivers()
            case let .failure(error):
                ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                ProgressHUD.shared.dismiss(afterDelay: 1.0)
                self.navigationController?.popViewController(animated: true)
            }
            
        }
        
    }
    
    private func fetchDrivers() {
        ApiController.shared.fetchDrivers { result in
            switch result {
            case let .success(drivers):
                self.allDrivers = drivers
                self.pickerView.reloadComponent(0)
                ProgressHUD.shared.dismiss(afterDelay: 1.0)
            case let .failure(error):
                ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                ProgressHUD.shared.dismiss(afterDelay: 2.0)
            }
        }
        
    }
}
