//
//  RegistrationViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/17/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    @IBOutlet var companyNameTextField: UITextField!
    @IBOutlet var companyActivityTextField: UITextField!
    @IBOutlet var companyRegistrationNumberTextField: UITextField!
    @IBOutlet var registrationExpirationNumberTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet var locationTextField: UITextField!
    @IBOutlet var licenseNameTextField: UITextField!
    @IBOutlet var licenseNumberTextField: UITextField!
    @IBOutlet var taxNumberTextField: UITextField!

    @IBOutlet var licenseExpirationTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var confirmPasswordTextField: UITextField!
    
    @IBOutlet var companyActivityView: UIView!
    @IBOutlet var licenseNameView: UIView!
    @IBOutlet var licenseNumberView: UIView!
    @IBOutlet var licenseExpirationView: UIView!
    
    @IBOutlet var locationPickerView: UIPickerView!
    
    var userType: UserType!
    var selectedCity = 0
    var index = 0
    var selectedId = 0
    
    private let dateFormatter = DateFormatter(withFormat: "dd-MM-yyyy", locale: Locale.current.identifier)
    
    private lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        datePicker.addTarget(self, action: #selector(didSelectDate), for: .valueChanged)
        return datePicker
    }()
    
    private var user: User {
        let user = User()
        user.companyName = companyNameTextField.text ?? ""
        user.companyActivity = userType == .driver ? nil : companyActivityTextField.text
        user.commercialNumber = companyRegistrationNumberTextField.text ?? ""
        user.commercialExpirationDate = registrationExpirationNumberTextField.text ?? ""
        user.taxNumber = taxNumberTextField.text ?? ""
        user.email = emailTextField.text ?? ""
        user.phone = phoneTextField.text ?? ""
        user.type = userType
        user.city = locationTextField.text ?? ""
        user.licenseName = userType == .factory ? nil : licenseNameTextField.text
        user.licenseNumber = userType == .factory ? nil : licenseNumberTextField.text
        user.licenseExpirationDate = userType == .factory ? nil : licenseExpirationTextField.text
        return user
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextFields()
    }
    
    // MARK: Private functions
    
    private func setupTextFields() {
        if userType == .driver {
            companyActivityView.isHidden = true
            companyActivityTextField.isHidden = true
        } else {
            licenseNameTextField.isHidden = true
            licenseNumberTextField.isHidden = true
            licenseExpirationTextField.isHidden = true
            licenseNameView.isHidden = true
            licenseNumberView.isHidden = true
            licenseExpirationView.isHidden = true
        }
        cityTextField.inputView = locationPickerView
        locationTextField.inputView = locationPickerView
        locationTextField.text = String(selectedId)
        registrationExpirationNumberTextField.inputView = datePicker
        licenseExpirationTextField.inputView = datePicker
    }
    
    private func validate(user: User, password: String, passwordConfirmation: String) -> String? {
        if user.companyName.isEmpty {
            return "error_invalid_company_name".localized
        } else if userType == .factory && user.companyActivity!.isEmpty {
            return "error_invalid_company_activity".localized
        } else if user.commercialNumber.count != 10 {
            return "error_invalid_commercial_number".localized
        } else if user.commercialExpirationDate.isEmpty {
            return "error_invalid_commercial_number_expiration_date".localized
        }else if user.taxNumber.isEmpty {
            return "error_invalid_tax_number".localized
        } else if !user.email.isValidEmail {
            return "error_invalid_email".localized
        } else if user.phone.isEmpty {
            return "error_invalid_phone".localized
        } else if password.isEmpty {
            return "error_invalid_password".localized
        } else if password != passwordConfirmation {
            return "error_passwords_do_not_match".localized
        } else if userType == .driver && user.licenseName!.isEmpty {
            return "error_invalid_license_name".localized
        } else if userType == .driver && user.licenseNumber!.isEmpty {
            return "error_invalid_license_number".localized
        } else if userType == .driver && user.licenseExpirationDate!.isEmpty {
            return "error_invalid_license_expiration_date".localized
        }
        return nil
    }
    
    private func presentRootViewController() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let rootViewController = mainStoryboard.instantiateInitialViewController() else {
            fatalError("Couldn't instantiate the initial view controller!")
        }
        ProgressHUD.shared.dismiss()
        presentAsKey(rootViewController, completion: nil)
    }
    
    @objc private func didSelectDate(_ sender: Any) {
        let selectedDate = dateFormatter.string(from: datePicker.date)
        if registrationExpirationNumberTextField.isFirstResponder {
            registrationExpirationNumberTextField.text = selectedDate
        } else if licenseExpirationTextField.isFirstResponder {
            licenseExpirationTextField.text = selectedDate
        }
    }
    
    // MARK: IB Actions
    
    @IBAction func didClickBack(_ sender: UIGestureRecognizer) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didClickRegister(_ sender: UIButton? = nil) {
        view.endEditing(true)
        let password = passwordTextField.text ?? ""
        let confirmPassword = confirmPasswordTextField.text ?? ""
        if let errorMessage = validate(user: user, password: password, passwordConfirmation: confirmPassword) {
            ProgressHUD.shared.show(.error, in: view, withMessage: errorMessage)
            ProgressHUD.shared.dismiss(afterDelay: 2.0)
        } else {
            ProgressHUD.shared.show(.pleaseWait, in: view)
            ApiController.shared.register(user: user, withPassword: password) { result in
                switch result {
                case let .success(user):
                    UserController.shared.currentUser = user
                    UserController.shared.registerDevice(for: user) { result in
                        switch result {
                        case .success:
                            self.presentRootViewController()
                        case let .failure(error):
                            ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                            ProgressHUD.shared.dismiss(afterDelay: 2.0)
                        }
                    }
                case let .failure(error):
                    ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0)
                }
            }
        }
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        switch textField.returnKeyType {
        case .next:
            if let nextResponder = view.viewWithTag(nextTag) as? UITextField {
                if nextResponder.isHidden {
                    return textFieldShouldReturn(nextResponder)
                } else {
                    nextResponder.becomeFirstResponder()
                }
            }
        case .go:
            didClickRegister()
        default:
            return true
        }
        return true
    }
    
    // MARK: UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if cityTextField.isFirstResponder{
            return 2
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        
        if component == 0 {
            if cityTextField.isFirstResponder {
                return Location.sharedInstance.getCities(countryIndex: selectedCity).count
            }else {
                return  Location.sharedInstance.getCities(countryIndex: selectedCity)[index].areas.count
            }
        } else {
            return Location.sharedInstance.getCountries().count
        }
    }
    
    // MARK: UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            if cityTextField.isFirstResponder {
                return Location.sharedInstance.getCities(countryIndex: selectedCity)[row].getName(showAra: Constants.showArabic)
            } else{
                return Location.sharedInstance.getCities(countryIndex: selectedCity)[index].areas[row].getName(showAra: Constants.showArabic)
            }
        }else {
            return Location.sharedInstance.getCountries()[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 1{
            selectedCity = row
            pickerView.reloadComponent(0)
        }else {
            if cityTextField.isFirstResponder {
                cityTextField.text = Location.sharedInstance.getCities(countryIndex: selectedCity)[row].getName(showAra: Constants.showArabic)
                index = row
            } else if locationTextField.isFirstResponder {
                locationTextField.text =  Location.sharedInstance.getCities(countryIndex: selectedCity)[index].areas[row].getName(showAra: Constants.showArabic)
                selectedId = Location.sharedInstance.getCities(countryIndex: selectedCity)[index].areas[row].id
                print("selectedId \(selectedId)")
            }
        }
    }
}
