//
//  PrintViewController.swift
//  Naql
//
//  Created by Admin on 2/15/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import UIKit
import WebKit

class PrintViewController: UIViewController,WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var printButton: UIButton!
    var url = "";
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        webView.navigationDelegate = self
        webView.load(URLRequest(url: URL(string: url)!))


    }
    
    @IBAction func printAction(_ sender: Any) {
        guard let urlCheck = webView.url
            else {return}
        
        let pi = UIPrintInfo.printInfo()
        pi.outputType = .general
        pi.jobName = urlCheck.absoluteString
        pi.orientation = .portrait
        pi.duplex = .longEdge
        
        let printController = UIPrintInteractionController.shared
        printController.printInfo = pi
        printController.printFormatter = webView.viewPrintFormatter()
        printController.present(animated: true, completionHandler: nil)
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        printButton.isHidden = false
        self.webView.isHidden = false
        loading.stopAnimating()
    }

}
