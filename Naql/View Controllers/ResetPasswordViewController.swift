//
//  PasswordViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/17/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ResetPasswordViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var emailTextField: UITextField!

    private var keyboardHeight: CGFloat = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerKeyboardStateObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        unregisterKeyboardStateObserver()
        super.viewWillDisappear(animated)
    }
    
    // MARK: Private functions
    
    private func registerKeyboardStateObserver() {
        IQKeyboardManager.shared.enable = false
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func unregisterKeyboardStateObserver() {
        NotificationCenter.default.removeObserver(self)
        IQKeyboardManager.shared.enable = true
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        let keyboardFrameKey = UIResponder.keyboardFrameEndUserInfoKey
        let keyboardSizeNSValue = notification.userInfo?[keyboardFrameKey] as? NSValue
        if let keyboardHeight = keyboardSizeNSValue?.cgRectValue.height {
            if keyboardHeight > self.keyboardHeight {
                view.frame.size.height -= (keyboardHeight - self.keyboardHeight)
                self.keyboardHeight = keyboardHeight
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        view.frame.size.height += keyboardHeight
        keyboardHeight = 0
    }
    private func validate(email: String) -> String? {
        if !email.isValidEmail {
            return "error_invalid_email".localized
        } else {
            return nil
        }
    }

    // MARK: IB Actions

    @IBAction func didClickBack(_ sender: UIGestureRecognizer) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func didClickSend(_ sender: UIButton? = nil) {
        view.endEditing(true)
        let email = emailTextField.text!
        if let errorMessage = validate(email: email) {
            ProgressHUD.shared.show(.error, in: view, withMessage: errorMessage)
            ProgressHUD.shared.dismiss(afterDelay: 2.0)
        } else {
            ProgressHUD.shared.show(.pleaseWait, in: view)
            ApiController.shared.sendPasswordResetEmail(to: email) { result in
                switch result {
                case .success:
                    ProgressHUD.shared.show(.success, in: self.view, withMessage: "message_reset_password_email_sent".localized)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0) {
                        self.navigationController?.popViewController(animated: true)
                    }
                case let .failure(error):
                    ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0)
                }
            }
        }
    }
}
