//
//  ComplaintsViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/24/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import MaterialComponents
import UIKit

class ComplaintsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var complaintsTableView: UITableView!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var newComplaintButton: MDCFloatingButton!

    private var refreshControl = UIRefreshControl()
    private var complaints: [Complaint] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        newComplaintButton.setElevation(ShadowElevation(rawValue: 2.0), for: .normal)
        complaintsTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refresh()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NewComplaintSegue", let viewController = segue.destination as? NewComplaintViewController {
            viewController.complaintsViewController = self
        }
    }

    func add(complaint: Complaint) {
        messageLabel.isHidden = true
        complaints.append(complaint)
        let complaintIndexPath = IndexPath(row: complaints.count - 1, section: 0)
        complaintsTableView.insertRows(at: [complaintIndexPath], with: .automatic)
    }

    // MARK: Private functions

    @objc private func refresh() {
        messageLabel.isHidden = true
        refreshControl.beginRefreshing()
        ApiController.shared.fetchComplaints { result in
            self.complaints.removeAll()
            switch result {
            case let .success(complaints):
                self.replaceComplaints(with: complaints)
            case let .failure(error):
                self.display(message: error)
            }
            self.complaintsTableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }

    private func replaceComplaints(with newComplaints: [Complaint]) {
        if !newComplaints.isEmpty {
            complaints.append(contentsOf: newComplaints)
            complaintsTableView.separatorStyle = .singleLine
        } else {
            display(message: "message_empty_compliants_list".localized)
        }
    }

    private func display(message: String) {
        messageLabel.text = message
        messageLabel.isHidden = false
        complaintsTableView.separatorStyle = .none
    }

    // MARK: UITableViewDataSource

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "ComplaintTableCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ComplaintTableCell
        cell.complaint = complaints[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return complaints.count
    }

    // MARK: UITableViewDelegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
