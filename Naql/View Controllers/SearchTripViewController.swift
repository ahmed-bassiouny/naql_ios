//
//  SearchTripViewController.swift
//  Naql
//
//  Created by Admin on 2/11/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import UIKit

class SearchTripViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tripIdTextField: UITextField!
    
    @IBOutlet weak var dateFromTextField: UITextField!
    
    @IBOutlet weak var dateToTextField: UITextField!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    
    private let dateFormatter = DateFormatter(withFormat: "dd-MM-yyyy",locale: "en_US_POSIX")
    var selectedTripId = 0
    
    private lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        datePicker.addTarget(self, action: #selector(didSelectDate), for: .valueChanged)
        return datePicker
    }()
    
    var list = [Order]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        dateFromTextField.inputView = datePicker
        dateToTextField.inputView = datePicker
        errorLabel.isHidden = true
        tripIdTextField.keyboardType = .asciiCapableNumberPad

    }
    
    @IBAction func search(_ sender: Any) {
        var id = tripIdTextField.text ?? ""
        let from = dateFromTextField.text ?? ""
        let to = dateToTextField.text ?? ""
        if id.isEmpty && from.isEmpty && to.isEmpty {
            ProgressHUD.shared.show(.error, in: self.view, withMessage: "field_required".localized)
            ProgressHUD.shared.dismiss(afterDelay: 1.0)
        }else if id.isEmpty {
            if from.isEmpty || to.isEmpty {
                ProgressHUD.shared.show(.error, in: self.view, withMessage: "field_required".localized)
                ProgressHUD.shared.dismiss(afterDelay: 1.0)
            }else {
                // valid
                fetchData(tripId: id, from: from, to: to)
            }
        }else {
            // get user
            let user = UserController.shared.currentUser
            if user?.type == .factory {
                fetchData(tripId: id, from: from, to: to)
            }else {
                if id.starts(with: String(user?.id ?? 0)) {
                   let index =  String(user?.id ?? 0).count
                    id = String(id.dropFirst(index))
                    if id.isEmpty {
                        ProgressHUD.shared.show(.error, in: self.view, withMessage: "trip_id_not_valid".localized)
                        ProgressHUD.shared.dismiss(afterDelay: 1.0)
                    }else {
                    fetchData(tripId: id, from: from, to: to)
                    }
                }else {
                    ProgressHUD.shared.show(.error, in: self.view, withMessage: "trip_id_not_valid".localized)
                    ProgressHUD.shared.dismiss(afterDelay: 1.0)
                }
            }
        }
    }
    
    @objc private func didSelectDate(_ sender: Any) {
        let selectedDate = dateFormatter.string(from: datePicker.date)
        if dateFromTextField.isFirstResponder {
            dateFromTextField.text = selectedDate
        } else if dateToTextField.isFirstResponder {
            dateToTextField.text = selectedDate
        }
    }
    
    // MARK : API LAYER
    
    private func fetchData(tripId:String,from:String,to:String)  {
        loading.startAnimating()
        self.errorLabel.isHidden = true
        ApiController.shared.fetchSearchForOrders(offset: 0, limit: 100, tripId: tripId, dateFrom: from, dateTo: to) { result in
            
            switch result {
            case let .failure(error):
                self.errorLabel.text = error
                self.loading.stopAnimating()
                self.errorLabel.isHidden = false
                break
            case let .success(orders):
                self.loading.stopAnimating()
                if orders.count > 0 {
                    self.list = orders
                    self.tableView.reloadData()
                }else {
                    self.errorLabel.text = "no_trip_found".localized
                    self.errorLabel.isHidden = false
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SearchOrderTableCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? OrderTableCell else {
            fatalError("The dequeued cell is not an instance of \(cellIdentifier).")
        }
        cell.order = list[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedTripId = list[indexPath.row].id
        performSegue(withIdentifier: "print", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "print", let viewController = segue.destination as? PrintViewController {
            if let user = UserController.shared.currentUser {
                if user.type == .factory{
                    viewController.url = "http://naaqll.com/naaqll.com/public/orders/company/drivers/\(selectedTripId)"
                }else {
                    viewController.url = "http://naaqll.com/naaqll.com/public/orders/drivers/\(selectedTripId)/\(user.id)"
                }
            }
        }
    }
    
}
