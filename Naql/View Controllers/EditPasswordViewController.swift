//
//  EditPasswordViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/20/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import MaterialComponents
import UIKit

class EditPasswordViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var currentPasswordTextField: MDCTextField!
    @IBOutlet var newPasswordTextField: MDCTextField!
    @IBOutlet var confirmPasswordTextField: MDCTextField!

    private var textFieldsControllers: [MDCTextInputControllerOutlined] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextFields()
    }

    // MARK: Private functions

    private func setupTextFields() {
        addController(to: currentPasswordTextField)
        addController(to: newPasswordTextField)
        addController(to: confirmPasswordTextField)
    }

    private func addController(to textField: MDCTextField) {
        let textFieldController = MDCTextInputControllerOutlined(textInput: textField)
        textFieldController.floatingPlaceholderActiveColor = .colorAccent
        textFieldController.activeColor = .colorAccent
        textFieldsControllers.append(textFieldController)
    }

    private func validate(_ currentPassword: String, _ newPassword: String, _ passwordConfirmation: String) -> String? {
        if currentPassword.isEmpty {
            return "error_current_password_field_empty".localized
        } else if newPassword.isEmpty {
            return "error_new_password_field_empty".localized
        } else if passwordConfirmation != newPassword {
            return "error_passwords_do_not_match".localized
        }
        return nil
    }

    // MARK: IBActions

    @IBAction func didClickSave(_ sender: UIButton? = nil) {
        view.endEditing(true)
        let currentPassword = currentPasswordTextField.text ?? ""
        let newPassword = newPasswordTextField.text ?? ""
        let confirmPassword = confirmPasswordTextField.text ?? ""
        if let errorMessage = validate(currentPassword, newPassword, confirmPassword) {
            ProgressHUD.shared.show(.error, in: view, withMessage: errorMessage)
            ProgressHUD.shared.dismiss(afterDelay: 2.0)
        } else {
            ProgressHUD.shared.show(.pleaseWait, in: view)
            ApiController.shared.changePassword(to: newPassword, oldPassword: currentPassword) { result in
                switch result {
                case .success:
                    ProgressHUD.shared.show(.success, in: self.view, withMessage: "message_password_changed".localized)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0) {
                        self.navigationController?.popViewController(animated: true)
                    }
                case let .failure(error):
                    ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0)
                }
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if textField.returnKeyType == .next, let nextResponder = view.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else if textField.returnKeyType == .go {
            didClickSave()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
}
