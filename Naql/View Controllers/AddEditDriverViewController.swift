//
//  AddEditDriverViewController.swift
//  Naql
//
//  Created by Admin on 3/7/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import UIKit

class AddEditDriverViewController: UIViewController {

    @IBOutlet weak var NameField: UITextField!
    @IBOutlet weak var PhoneField: UITextField!
    
    @IBOutlet weak var deleteButton: UIButton!
    
    var driver = Driver()
    var passDataPro:PassDataProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if driver.id > 0 {
            setData()
            deleteButton.isHidden = false
        } else {
            deleteButton.isHidden = true
        }
        
    }
    
    
    @IBAction func save(_ sender: Any) {
        if validInputs() {
            saveDriver()
        }
    }
    
    func setData() {
        NameField.text = driver.name
        PhoneField.text = driver.phone
        
    }
    
    func validInputs() -> Bool {
        if NameField.text?.isEmpty ?? true || PhoneField.text?.isEmpty ?? true {
            ProgressHUD.shared.show(.error, in: view, withMessage: "all_field_required".localized)
            ProgressHUD.shared.dismiss(afterDelay: 1.0)
            return false
        }
        return true
    }
    
    func saveDriver()  {
        driver.name = NameField.text ?? ""
        driver.phone = PhoneField.text ?? ""
        
        view.endEditing(true)
        ProgressHUD.shared.show(.pleaseWait, in: view)
        ApiController.shared.addOrEditDriver(driver) { result in
            switch result {
            case let .success(driver):
                if let pro = self.passDataPro {
                    pro.passData(genericClass: driver)
                }
                ProgressHUD.shared.dismiss(afterDelay: 1.0) {
                    self.navigationController?.popViewController(animated: true)
                }
            case let .failure(error):
                ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                ProgressHUD.shared.dismiss(afterDelay: 1.0)
            }
        }
        
    }
    
    @IBAction func deleteDriver(_ sender: Any) {
        let uiAlert = UIAlertController(title: "delete".localized, message: "are_you_sure".localized, preferredStyle: UIAlertController.Style.alert)
        
        uiAlert.addAction(UIAlertAction(title: "yes".localized, style: .default, handler: { action in
            ProgressHUD.shared.show(.pleaseWait, in: self.view)
            ApiController.shared.deleteDriver(driverId:self.driver.id) { result in
                switch result {
                case .success(_):
                    ProgressHUD.shared.show(.success, in: self.view, withMessage: "done".localized)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0) {
                        self.navigationController?.popViewController(animated: true)
                        if let pro = self.passDataPro {
                            pro.passData(genericClass: "")
                        }
                    }
                   
                case let .failure(message):
                    ProgressHUD.shared.show(.success, in: self.view, withMessage: message)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0)
                }
            }
        }))
        
        uiAlert.addAction(UIAlertAction(title: "no".localized, style: .cancel, handler: { action in
        }))
        self.present(uiAlert, animated: true, completion: nil)
    }

}
