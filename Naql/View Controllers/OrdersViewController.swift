//
//  OrdersViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/21/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import MaterialComponents
import UIKit

class OrdersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var ordersTableView: UITableView!
    @IBOutlet var emptyListLabel: UILabel!
    @IBOutlet var newOrderButton: UIButton!

    private var orders: [Order] = []
    private var isFetchInProgress = false
    private var didReachLastPage = false
    private var shouldRefresh = false
    private var currentPage = 0
    private let pageSize = 10

    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        newOrderButton.isHidden = UserController.shared.currentUser?.type == .driver
        //newOrderButton.setElevation(ShadowElevation(rawValue: 2.0), for: .normal)
        ordersTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
       // fetchrOrders()
        
    }

    override func viewDidAppear(_ animated: Bool) {
        refresh()
    }
    @available(iOS, deprecated: 9.0)
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OrderDetailsSegue" {
            let viewController = segue.destination as! OrderDetailsViewController
            viewController.order = sender as? Order
            viewController.ordersViewController = self
        } else if segue.identifier == "NewOrderSegue", let viewController = segue.destination as? NewOrderViewController {
            viewController.ordersViewController = self
        }
    }

    func add(order: Order) {
        emptyListLabel.isHidden = true
        orders.insert(order, at: 0)
        let orderIndexPath = IndexPath(row: 0, section: 0)
        ordersTableView.insertRows(at: [orderIndexPath], with: .automatic)
    }

    func cancelOrder(withId orderId: Int) {
        if let order = orders.first(where: { $0.id == orderId }) {
            order.status = .canceled
            if let index = orders.firstIndex(of: order) {
                ordersTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            }
        }
    }

    // MARK: Private functions

    @objc private func refresh() {
        refreshControl.beginRefreshing()
        didReachLastPage = false
        shouldRefresh = true
        if !isFetchInProgress {
            currentPage = 0
            fetchrOrders()
        }
    }

    private func fetchrOrders() {
        guard !isFetchInProgress, !didReachLastPage else {
            return
        }
        isFetchInProgress = true
        let status: [OrderStatus] = [.running, .canceled]
        let forDriver = UserController.shared.currentUser?.type == .driver
        let past = title == "title_past_orders".localized
        ApiController.shared.fetchOrders(forDriver: forDriver, past: past, page: currentPage, limit: pageSize, status: status) { result in
            if self.shouldRefresh {
                self.orders.removeAll()
                self.ordersTableView.reloadData()
                self.shouldRefresh = false
                if self.currentPage != 0 {
                    self.fetchrOrders()
                    return
                }
            }
            switch result {
            case let .failure(error):
                self.showError(error)
            case let .success(orders):
                if orders.1 == 0 {
                    self.showError(nil)
                } else {
                    if !orders.0.isEmpty {
                        self.orders.append(contentsOf: orders.0)
                        let indexPaths = self.calculateIndexPathsToReload(from: orders.0)
                        self.ordersTableView.insertRows(at: indexPaths, with: .automatic)
                    }
                    if orders.1 <= self.orders.count && !self.didReachLastPage {
                        self.didReachLastPage = true
                        let indexPaths = [IndexPath(row: self.orders.count, section: 0)]
                        self.ordersTableView.deleteRows(at: indexPaths, with: .automatic)
                    }
                }
                self.currentPage += 1
            }
            self.isFetchInProgress = false
            self.refreshControl.endRefreshing()
        }
    }

    private func calculateIndexPathsToReload(from newOrders: [Order]) -> [IndexPath] {
        let startIndex = orders.count - newOrders.count
        let endIndex = startIndex + newOrders.count
        return (startIndex ..< endIndex).map { IndexPath(row: $0, section: 0) }
    }

    private func showError(_ errorMessage: String?) {
        emptyListLabel.isHidden = false
        emptyListLabel.text = errorMessage ?? "message_empty_orders_list".localized
        didReachLastPage = true
        let indexPaths = [IndexPath(row: self.orders.count, section: 0)]
        ordersTableView.deleteRows(at: indexPaths, with: .automatic)
    }

    // MARK: UITableViewDelegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != orders.count {
            performSegue(withIdentifier: "OrderDetailsSegue", sender: orders[indexPath.row])
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }

    // MARK: UITableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return didReachLastPage ? orders.count : orders.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "OrderTableCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? OrderTableCell else {
            fatalError("The dequeued cell is not an instance of \(cellIdentifier).")
        }
        if indexPath.row == orders.count {
            cell.order = nil
            if !didReachLastPage && indexPath.row != 0 {
                fetchrOrders()
            }
        } else {
            cell.order = orders[indexPath.row]
        }
        return cell
    }
}
