//
//  Profile ViewCoontroller.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/20/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    @IBOutlet var companyNameLabel: UILabel!
    @IBOutlet var commercialNumberLabel: UILabel!
    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var companyActivityLabel: UILabel!
    @IBOutlet var companyActivityView: UIView!
  
    @IBOutlet weak var pointLabel: UILabel!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fillUserInfo()
    }

    // MARK: Private functions

    private func fillUserInfo() {
        guard let currentUser = UserController.shared.currentUser else {
            UserController.shared.signOut(from: self)
            return
        }
        companyNameLabel.text = currentUser.companyName
        commercialNumberLabel.text = currentUser.commercialNumber
        phoneNumberLabel.text = currentUser.phone
        emailLabel.text = currentUser.email
        companyActivityView.isHidden = currentUser.type == .driver
        companyActivityLabel.text = currentUser.companyActivity
        if currentUser.type == .driver {
            pointLabel.isHidden = true
        }else {
            pointLabel.text = String(format: "you_points".localized, currentUser.points)
        }
    }
}
