//
//  MoreOrderDetailsViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 12/6/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

class MoreOrderDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var orderIdLabel: UILabel!
    @IBOutlet var emptyListLabel: UILabel!
    @IBOutlet var driversTableView: UITableView!

    var order: Order!

    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "CompanyHeader", bundle: nil)
        driversTableView.register(nib, forHeaderFooterViewReuseIdentifier: "CompanyHeaderView")
        fillInfo()
    }

    // MARK: Private functions

    private func fillInfo() {
//        for i in 1 ... 5 {
//            let company = DriversCompany()
//            company.companyName = "Company number \(i)"
//            company.numberOfShipments = Int.random(in: 1 ... 10)
//            for j in 1 ... 5 {
//                let driver = Driver()
//                driver.name = "Driver \(j)"
//                driver.phone = "\(j)38\(j)75833\(j)"
//                driver.carType = "Something"
//                driver.carNumber = "Something else"
//                company.drivers.append(driver)
//            }
//            order.companies.append(company)
//        }
//        driversTableView.reloadData()
        if order.companies.isEmpty && !order.drivers.isEmpty {
            let driverCompany = DriversCompany()
            driverCompany.drivers = order.drivers
            order.companies.append(driverCompany)
            emptyListLabel.isHidden = true
            driversTableView.separatorStyle = .none
        }else if order.companies.isEmpty {
            emptyListLabel.isHidden = false
            driversTableView.separatorStyle = .none
        }
        orderIdLabel.text = String(format: "label_order_id".localized, order.id)
    }

    // MARK: UITableViewDelegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    // MARK: UITableViewDataSource

    func numberOfSections(in tableView: UITableView) -> Int {
        return order.companies.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order.companies[section].drivers.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CompanyHeaderView") as! CompanyHeaderView
        let company = order.companies[section]
        headerView.companyNameLabel.text = company.companyName
        headerView.numberOfShipmentsLabel.text = String(format: "label_company_shipments".localized, company.drivers.count)
        return headerView
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "DriverTableCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DriverTableCell else {
            fatalError("The dequeued cell is not an instance of \(cellIdentifier).")
        }
        cell.driver = order.companies[indexPath.section].drivers[indexPath.row]
        return cell
    }
}
