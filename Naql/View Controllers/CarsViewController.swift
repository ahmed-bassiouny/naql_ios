//
//  CarsViewController.swift
//  Naql
//
//  Created by Admin on 1/22/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import UIKit
import MaterialComponents


class CarsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
   
    
    @IBOutlet weak var createButton: UIButton!
    
    @IBOutlet weak var carTable: UITableView!
    private var refreshControl = UIRefreshControl()

    var list :[Car] = []
   
    override func viewDidLoad() {
        super.viewDidLoad()
        carTable.dataSource = self
        carTable.delegate = self
        
        carTable.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        refresh()
    }
    
    
    @objc private func refresh() {
        refreshControl.beginRefreshing()
        ApiController.shared.fetchCars { result in
            self.list.removeAll()
            switch result {
            case let .success(cars):
                self.list = cars
                self.carTable.reloadData()
            case let .failure(error):
                ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                ProgressHUD.shared.dismiss(afterDelay: 2.0)
            }
            
            self.refreshControl.endRefreshing()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "car", for: indexPath) as! CarTableViewCell
        cell.carItem = list[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != list.count {
            performSegue(withIdentifier: "car_details", sender: list[indexPath.row])
        }
        //tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "car_details" {
            let viewController = segue.destination as! NewCarViewController
            viewController.car = sender as? Car ?? Car()
        }
    }
    
}
