//
//  EditProfileViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/20/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import MaterialComponents
import UIKit

class EditProfileViewController: UIViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    @IBOutlet var companyNameTextField: MDCTextField!
    @IBOutlet var companyActivityTextField: MDCTextField!
    @IBOutlet var commercialNumberTextField: MDCTextField!
    @IBOutlet var phoneNumberTextField: MDCTextField!
    @IBOutlet var locationTextField: UITextField!

    @IBOutlet var locationPickerView: UIPickerView!

    private var textFieldsControllers: [MDCTextInputControllerOutlined] = []

    private var user: User {
        let user = UserController.shared.currentUser!
        user.companyName = companyNameTextField.text ?? ""
        user.companyActivity = companyActivityTextField.text ?? ""
        user.commercialNumber = commercialNumberTextField.text ?? ""
        user.phone = phoneNumberTextField.text ?? ""
        user.city = locationTextField.text ?? ""
        return user
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextFields()
        fillUserInfo()
        locationTextField.inputView = locationPickerView
    }

    // MARK: Private functions

    private func fillUserInfo() {
        guard let currentUser = UserController.shared.currentUser else {
            UserController.shared.signOut(from: self)
            return
        }
        companyNameTextField.text = currentUser.companyName
        companyActivityTextField.text = currentUser.companyActivity
        commercialNumberTextField.text = currentUser.commercialNumber
        phoneNumberTextField.text = currentUser.phone
        companyActivityTextField.isHidden = currentUser.type == .driver
        for (index, city) in Location.sharedInstance.getRegions()[0].enumerated() {
            /*if city == currentUser.city {
                locationPickerView.selectRow(index, inComponent: 0, animated: false)
                locationTextField.text = city
                break
            }*/
        }
    }

    private func setupTextFields() {
        addController(to: companyNameTextField)
        addController(to: companyActivityTextField)
        addController(to: commercialNumberTextField)
        addController(to: phoneNumberTextField)
    }

    private func addController(to textField: MDCTextField) {
        let textFieldController = MDCTextInputControllerOutlined(textInput: textField)
        textFieldController.floatingPlaceholderActiveColor = .colorAccent
        textFieldController.activeColor = .colorAccent
        textFieldsControllers.append(textFieldController)
    }

    private func validate(user: User) -> String? {
        if user.companyName.isEmpty {
            return "error_invalid_company_name".localized
        } else if user.commercialNumber.isEmpty {
            return "error_invalid_commercial_number".localized
        } else if user.phone.isEmpty {
            return "error_invalid_phone".localized
        }
        return nil
    }

    // MARK: IBActions

    @IBAction func didClickSave(_ sender: UIButton? = nil) {
        view.endEditing(true)
        if let errorMessage = validate(user: user) {
            ProgressHUD.shared.show(.error, in: view, withMessage: errorMessage)
            ProgressHUD.shared.dismiss(afterDelay: 2.0)
        } else {
            ProgressHUD.shared.show(.pleaseWait, in: view)
            ApiController.shared.editProfile(for: user) { result in
                switch result {
                case let .success(user):
                    UserController.shared.currentUser = user
                    ProgressHUD.shared.show(.success, in: self.view, withMessage: "message_profile_saved".localized)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0) {
                        self.navigationController?.popViewController(animated: true)
                    }
                case let .failure(error):
                    ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0)
                }
            }
        }
    }

    // MARK: UITextFieldDelegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let currentUser = UserController.shared.currentUser!
        let nextTag = textField.tag + (currentUser.type == .driver && textField.tag == 0 ? 2 : 1)
        if textField.returnKeyType == .next, let nextResponder = view.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }

    // MARK: UIPickerViewDataSource

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Location.sharedInstance.getRegions().count
    }

    // MARK: UIPickerViewDelegate

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Location.sharedInstance.getRegions()[0]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        locationTextField.text = Location.sharedInstance.getRegions()[0]
    }
}
