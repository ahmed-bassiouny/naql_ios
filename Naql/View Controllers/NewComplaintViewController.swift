//
//  Register Complaint.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/26/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import MaterialComponents
import UIKit

class NewComplaintViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var phoneTextField: MDCTextField!
    @IBOutlet var emailTextField: MDCTextField!
    @IBOutlet var titleTextField: MDCTextField!
    @IBOutlet var complaintTextView: MDCMultilineTextField!

    private var textFieldsControllers: [MDCTextInputControllerBase] = []

    private var complaint: Complaint {
        let complaint = Complaint()
        let currentUser = UserController.shared.currentUser!
        complaint.userID = "\(currentUser.id)"
        complaint.name = currentUser.companyName
        complaint.phone = phoneTextField.text ?? ""
        complaint.email = emailTextField.text ?? ""
        complaint.title = titleTextField.text ?? ""
        complaint.compliant = complaintTextView.text ?? ""
        return complaint
    }

    var complaintsViewController: ComplaintsViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextFields()
    }

    // MARK: Private functions

    private func setupTextFields() {
        addController(to: phoneTextField)
        addController(to: emailTextField)
        addController(to: titleTextField)
        addController(to: complaintTextView)
    }

    private func addController(to textField: UIView) {
        let textFieldController: MDCTextInputControllerBase
        if let textField = textField as? MDCMultilineTextField {
            textFieldController = MDCTextInputControllerOutlinedTextArea(textInput: textField)
        } else if let textField = textField as? MDCTextField {
            textFieldController = MDCTextInputControllerOutlined(textInput: textField)
        } else {
            return
        }
        textFieldController.floatingPlaceholderActiveColor = .colorAccent
        textFieldController.activeColor = .colorAccent
        textFieldsControllers.append(textFieldController)
    }

    private func validate(complaint: Complaint) -> String? {
        if complaint.phone.isEmpty {
            return "error_invalid_phone".localized
        } else if !complaint.email.isValidEmail {
            return "error_invalid_email".localized
        } else if complaint.title.isEmpty {
            return "error_invalid_title".localized
        } else if complaint.compliant.isEmpty {
            return "error_invalid_complaint".localized
        }
        return nil
    }

    // MARK: IBActions

    @IBAction func didClickSend(_ sender: UIButton) {
        view.endEditing(true)
        if let errorMessage = validate(complaint: complaint) {
            ProgressHUD.shared.show(.error, in: view, withMessage: errorMessage)
            ProgressHUD.shared.dismiss(afterDelay: 2.0)
        } else {
            ProgressHUD.shared.show(.pleaseWait, in: view)
            ApiController.shared.addComplaint(complaint) { result in
                switch result {
                case let .success(message):
                    ProgressHUD.shared.show(.success, in: self.view, withMessage: message)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0) {
                        self.complaintsViewController?.add(complaint: self.complaint)
                        self.navigationController?.popViewController(animated: true)
                    }
                case let .failure(error):
                    ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0)
                }
            }
        }
    }

    // MARK: UITextFieldDelegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if textField.returnKeyType == .next, let nextResponder = view.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}
