//
//  MoreViewController.swift
//  Naql
//
//  Created by Admin on 2/8/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    var listName = [String]()
    var listIcon = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        guard let currentUser = UserController.shared.currentUser else {
            UserController.shared.signOut(from: self)
            return
        }
        if currentUser.type == .driver {
            listName = ["about_us".localized,"search_for_bill".localized,"notification".localized,"cars".localized,"drivers".localized,"change_language".localized]
            listIcon = [#imageLiteral(resourceName: "About Us Icon"),#imageLiteral(resourceName: "search"),#imageLiteral(resourceName: "notify_icon"),#imageLiteral(resourceName: "car_icon"),#imageLiteral(resourceName: "delivery"),#imageLiteral(resourceName: "language_icon")]
        }else {
            listName = ["about_us".localized,"search_for_bill".localized,"package_view".localized,"change_language".localized]
            listIcon = [#imageLiteral(resourceName: "About Us Icon"),#imageLiteral(resourceName: "search"),#imageLiteral(resourceName: "package_icon"),#imageLiteral(resourceName: "language_icon")]
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "more_table"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! MoreTableViewCell
        cell.setData(name: listName[indexPath.row], icon: listIcon[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch listName[indexPath.row] {
        case "about_us".localized:
            performSegue(withIdentifier: "show_about", sender: self)
            break
        case "package_view".localized:
            let storyBoard = UIStoryboard(name: "Profile", bundle: nil)
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "Package")
            self.navigationController?.pushViewController(mainViewController, animated: true)
            break
        case "cars".localized:
            let storyBoard = UIStoryboard(name: "Car", bundle: nil)
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "Cars")
            self.navigationController?.pushViewController(mainViewController, animated: true)
            break
        case "notification".localized:
            let storyBoard = UIStoryboard(name: "Profile", bundle: nil)
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "Notification")
            self.navigationController?.pushViewController(mainViewController, animated: true)
            break
        case "search_for_bill".localized:
            let storyBoard = UIStoryboard(name: "Orders", bundle: nil)
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "Search")
            self.navigationController?.pushViewController(mainViewController, animated: true)
            break
        case "change_language".localized:
            LanguageUtils.changeLanguage(vc: self)
            break;
        case "drivers".localized:
            let storyBoard = UIStoryboard(name: "Driver", bundle: nil)
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "Drivers")
            self.navigationController?.pushViewController(mainViewController, animated: true)
        default:
            return
        }
    }
    
}
