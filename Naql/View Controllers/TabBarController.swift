//
//  TabBarController.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/19/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        title = tabBar.selectedItem?.title
        if UserController.shared.currentUser?.type == .driver {
            viewControllers?[1].title = "title_past_orders".localized
        } else if viewControllers?.count == 5 {
            viewControllers?.remove(at: 1)
        }
    }

    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        title = item.title
    }

    // MARK: IB Actions

    @IBAction func didClickLogout(_ sender: UIButton) {
        UserController.shared.signOut(from: self)
    }
}
