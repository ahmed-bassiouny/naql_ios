//
//  NotificationViewController.swift
//  Naql
//
//  Created by Admin on 1/11/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import UIKit
import Firebase

class NotificationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NotificationDelegate {
    
    
    var arr = [String]()
    var selectedIds : [Int] = []
    var switchStatus : [Bool] = []
    var cities = [City]()
    
    @IBOutlet weak var notificationTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arr = Location.sharedInstance.getCountries()
        getSelectedCitiesFromCach()
        setSelectedCitiesInView()
        notificationTableView.dataSource = self
        notificationTableView.delegate = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notification_cell", for: indexPath) as! NotificationTableViewCell
        cell.delegate = self
        // replace cityId:indexPath.row with city id
        cell.setData(index:indexPath.row ,city: cities[indexPath.row],status:switchStatus[indexPath.row])
        return cell
    }
    
    
    /*func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return arr[section]
    }*/
    
    /*func numberOfSections(in tableView: UITableView) -> Int {
        
        return arr.count
    }*/
    // add select id of city to array
    func add(_ id: Int,_ index: Int) {
        selectedIds.append(id)
        switchStatus[index] = true
    }
    
    // remove select id of city to array
    func remove(_ id: Int,_ index: Int) {
        switchStatus[index] = false
        if let indexId = selectedIds.index(of:id) {
            selectedIds.remove(at: indexId)
        }
    }
    
    // get selected city saved from cach
    func getSelectedCitiesFromCach()  {
        selectedIds = UserController.shared.notification
    }
    
    // save selected city to cach
    @IBAction func save(_ sender: Any) {
        // get cach array
        let lastIds = UserController.shared.notification
        // unsubscribe last ids
        for id in lastIds {
            Messaging.messaging().unsubscribe(fromTopic: String(id))
        }
        // subscribe new ids
        for id in selectedIds {
            Messaging.messaging().subscribe(toTopic: String(id))
        }
        UserController.shared.notification = selectedIds
        navigationController?.popViewController(animated: true)
    }
    
    
    // set select item from cach
    func setSelectedCitiesInView()  {
        cities.append(contentsOf: Location.sharedInstance.getCities(countryIndex: 0))
        cities.append(contentsOf: Location.sharedInstance.getCities(countryIndex: 1))
        for (i,city) in cities.enumerated(){
             self.switchStatus.append(false)
            for id in selectedIds {
                if id == city.id{
                    self.switchStatus[i] = true
                }
            }
        }
        
    }
    
}
