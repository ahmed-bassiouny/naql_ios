//
//  LoginViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/17/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    private func presentRootViewController() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let rootViewController = mainStoryboard.instantiateInitialViewController() else {
            fatalError("Couldn't instantiate the initial view controller!")
        }
        ProgressHUD.shared.dismiss()
        presentAsKey(rootViewController, completion: nil)
    }
   
    
    
    private func validate(email: String, password: String) -> String? {
        if !email.isValidEmail {
            return "error_invalid_email".localized
        } else if password.isEmpty {
            return "error_invalid_password".localized
        } else {
            return nil
        }
    }
    
    // MARK: IBActions
    
    @IBAction func didClickSignIn(_ sender: UIButton? = nil) {
        view.endEditing(true)
        let email = emailTextField.text!
        let password = passwordTextField.text!
        if let errorMessage = validate(email: email, password: password) {
            ProgressHUD.shared.show(.error, in: view, withMessage: errorMessage)
            ProgressHUD.shared.dismiss(afterDelay: 2.0)
        } else {
            ProgressHUD.shared.show(.pleaseWait, in: view)
            UserController.shared.signIn(email: email, password: password) { result in
                switch result {
                case .success:
                    self.presentRootViewController()
                case let .failure(error):
                    ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0)
                }
            }
        }
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if textField.returnKeyType == .next, let nextResponder = view.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            didClickSignIn()
        }
        return true
    }
    
    @IBAction func changeLanguage(_ sender: Any) {
        
        LanguageUtils.changeLanguage(vc: self)
        
    }
    
}
