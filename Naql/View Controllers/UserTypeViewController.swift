//
//  UserTypeViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/17/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

class UserTypeViewController: UIViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let registrationViewController = segue.destination as? RegistrationViewController {
            switch segue.identifier {
            case "TransportCompaniesSegue":
                registrationViewController.userType = .driver
            default:
                registrationViewController.userType = .factory
            }
        }
    }
}
