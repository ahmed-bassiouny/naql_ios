//
//  NewOrderViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/23/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import GooglePlacePicker
import MaterialComponents
import TextFieldEffects
import UIKit

@available(iOS, deprecated: 9.0)
class NewOrderViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, GMSPlacePickerViewControllerDelegate {
    @IBOutlet var companyNameTextField: MDCTextField!
    // from
    @IBOutlet var originRegionTextField: UITextField!
    @IBOutlet var originCityTextField: UITextField!
    @IBOutlet var originAddressTextField: MDCTextField!
    // to
    @IBOutlet var destinationRegionTextField: UITextField!
    @IBOutlet var destinationCityTextField: UITextField!
    @IBOutlet var destinationAddressTextField: MDCTextField!
    
    // return
    @IBOutlet var returnRegionTextField: UITextField!
    @IBOutlet var returnCityTextField: UITextField!
    @IBOutlet var returnAddressTextField: MDCTextField!
    @IBOutlet weak var returnRegionView: UIView!
    @IBOutlet weak var returnCityView: UIView!
    //-----
    @IBOutlet var requireMarinaEntrySwitch: UISwitch!
    //@IBOutlet var hasMarinaAuthorizationSwitch: UISwitch!
    //@IBOutlet var hasMarinaAuthorizationLabel: UILabel!
    @IBOutlet var orderTypeTextField: UITextField!
    @IBOutlet var tripTypeTextField: UITextField!
    @IBOutlet var numberOfShimpmentsTextField: MDCTextField!
    @IBOutlet var weightTextField: MDCTextField!
    @IBOutlet var workingHoursStartTextField: MDCTextField!
    @IBOutlet var workingHoursEndTextField: MDCTextField!
    @IBOutlet var holidaysTextField: MDCTextField!
    @IBOutlet var paymentMethodsTextField: MDCTextField!
    @IBOutlet var proposedPriceTextField: PaddedMDCTextField!
    @IBOutlet var currencyLabel: UILabel!
    @IBOutlet var pickerView: UIPickerView!
    
    @IBOutlet weak var saveCostTextField: UITextField!
    
    
    @IBOutlet weak var typeOfLoad: MDCTextField!
    
    @IBOutlet weak var descOfLoad: MDCTextField!
    
    /** private lazy var datePicker: UIDatePicker = {
     let datePicker = UIDatePicker()
     datePicker.datePickerMode = .time
     datePicker.minuteInterval = 30
     datePicker.addTarget(self, action: #selector(didSelectTime), for: .valueChanged)
     return datePicker
     }()*/
    
    private let timeFormatter = DateFormatter(withFormat: "hh:mm a", locale: Locale.current.identifier)
    private var textFieldsControllers: [MDCTextInputControllerOutlined] = []
    private var placePickerPresented :LocationType?
    
    private var originCoordinates: Coordinates?
    private var destinationCoordinates: Coordinates?
    private var returnCoordinates: Coordinates?
    
    var ordersViewController: OrdersViewController?
    
    var roundTrip = false
    var saveCostValue = -1
    var selectedCity = 0
    var orderType = -1
    var topic = 0;
    var paymentMethod = -1
    private var order: Order {
        let order = Order()
        order.companyName = companyNameTextField.text ?? ""
        order.fromCity = String(originId)
        order.fromAddress = originAddressTextField.text ?? ""
        order.fromLat = originCoordinates?.latitude ?? 0.0
        order.fromLng = originCoordinates?.longitude ?? 0.0
        order.toCity = String(distinationId)
        order.toAddress = destinationAddressTextField.text ?? ""
        order.toLat = destinationCoordinates?.latitude ?? 0.0
        order.toLng = destinationCoordinates?.longitude ?? 0.0
        order.orderType = orderType
        order.tripType = roundTrip ? 1 : 0 // tripTypeTextField.text ?? ""
        order.numberOfShipmentsAvailable = Int(numberOfShimpmentsTextField.text ?? "0") ?? 0
        order.loadInTon = weightTextField.text ?? ""
        order.fromTime = selectedTimeFromIndex
        order.toTime = selectedTimeToIndex
        order.holiday = holidaysTextField.text ?? ""
        order.price = proposedPriceTextField.text ?? ""
        order.topic = topic
        order.requiresSeaport = requireMarinaEntrySwitch.isOn ? "1" : "0"
        order.paymentMethod = paymentMethod
        if roundTrip {
            order.returnCity = String(returnId)
            order.returnAddress = returnAddressTextField.text ?? ""
            order.returnLat = returnCoordinates?.latitude ?? 0.0
            order.returnLng = returnCoordinates?.longitude ?? 0.0
        }
        order.saveCost = saveCostValue
        order.typeOfLoad = typeOfLoad.text ?? ""
        order.descOfLoad = descOfLoad.text ?? ""
        return order
    }
    var originIndex = 0
    var originId = 0
    var distinationIndex = 0
    var distinationId = 0
    var returnIndex = 0
    var returnId = 0
    var selectedTimeFromIndex = -1
    var selectedTimeToIndex = -1
    
    var times = Constants.times
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let _ = UserController.shared.currentUser else {
            UserController.shared.signOut(from: self)
            return
        }
        //Constants.showArabic = currentUser.languageArabic
        setupTextFields()
        
    }
    
    // MARK: Private functions
    
    
    private func setupTextFields() {
        addController(to: companyNameTextField)
        addController(to: originAddressTextField)
        addController(to: destinationAddressTextField)
        addController(to: numberOfShimpmentsTextField)
        addController(to: weightTextField)
        addController(to: workingHoursStartTextField)
        addController(to: workingHoursEndTextField)
        addController(to: holidaysTextField)
        addController(to: proposedPriceTextField)
        addController(to: typeOfLoad)
        addController(to: descOfLoad)
        addController(to: paymentMethodsTextField)
        proposedPriceTextField.endPadding = currencyLabel.frame.width - 5
        originRegionTextField.inputView = pickerView
        originCityTextField.inputView = pickerView
        destinationRegionTextField.inputView = pickerView
        destinationCityTextField.inputView = pickerView
        returnRegionTextField.inputView = pickerView
        returnCityTextField.inputView = pickerView
        orderTypeTextField.inputView = pickerView
        tripTypeTextField.inputView = pickerView
        saveCostTextField.inputView = pickerView
        paymentMethodsTextField.inputView = pickerView
        originAddressTextField.inputView = UIView()
        destinationAddressTextField.inputView = UIView()
        returnAddressTextField.inputView = UIView()
        workingHoursStartTextField.inputView = pickerView
        workingHoursEndTextField.inputView = pickerView
        proposedPriceTextField.clearButtonMode = .never
        companyNameTextField.text = UserController.shared.currentUser?.companyName
        
        
        // hide return views
        checkTripType()
        // select trip type is one way
        tripTypeTextField.text = Constants.tripTypes[0]
        
        // get cach times from user default
        selectedTimeFromIndex = UserDefaults.standard.integer(forKey: "from_time")
        selectedTimeToIndex = UserDefaults.standard.integer(forKey: "to_time")
        workingHoursStartTextField.text = times[selectedTimeFromIndex]
        workingHoursEndTextField.text = times[selectedTimeToIndex]
        
    }
    
    private func addController(to textField: MDCTextField) {
        let textFieldController = MDCTextInputControllerOutlined(textInput: textField)
        textFieldController.floatingPlaceholderActiveColor = .darkGray
        textFieldController.activeColor = .darkGray
        textFieldController.inlinePlaceholderColor = .darkGray
        textFieldController.floatingPlaceholderNormalColor = .darkGray
        textFieldController.normalColor = .darkGray
        textFieldsControllers.append(textFieldController)
    }
    
    private func presentPlacePicker(type: LocationType) {
        placePickerPresented = type
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
        UIApplication.shared.statusBarStyle = .default
    }
    
    private func validate(order: Order) -> String? {
        if order.companyName.isEmpty {
            return "error_invalid_company_name".localized
        }/* else if originCoordinates == nil {
             return "error_invalid_origin_address".localized
             } else if destinationCoordinates == nil {
             return "error_invalid_destination_address".localized
             }else if returnCoordinates == nil && roundTrip{
             return "error_invalid_return_address".localized
         }*/ else if order.fromCity.isEmpty || order.fromCity == "0" {
            return "error_invalid_origin_city".localized
        } else if order.toCity.isEmpty || order.toCity == "0" {
            return "error_invalid_destination_city".localized
        }else if ( order.returnCity.isEmpty || order.returnCity == "0" ) && roundTrip {
            return "error_invalid_return_city".localized
        } else if order.orderType == -1 {
            return "error_invalid_order_type".localized
        }/* else if order.tripType.isEmpty {
             return "error_invalid_trip_type".localized
         } */else if order.numberOfShipmentsAvailable == 0 {
            return "error_invalid_number_of_shipments".localized
        } else if order.loadInTon.isEmpty {
            return "error_invalid_weight".localized
        } else if order.price.isEmpty {
            return "error_invalid_price".localized
        } else if paymentMethod == -1 {
            return "error_payment_method".localized
        }else if order.saveCost == -1 {
            return "error_save_cost".localized
        }else if Int(order.price) ?? 0 > 500000 {
            return "invalid_price".localized
        }else if ( Int(order.loadInTon) ?? 0 > 150 || (Int(order.loadInTon) ?? 0 > 30 &&  orderType != 3) ) {
            return "invalid_load_size".localized
        }
        return nil
    }
    
    /* @objc private func didSelectTime(_ sender: Any) {
     let selectedTime = timeFormatter.string(from: datePicker.date)
     if workingHoursStartTextField.isFirstResponder {
     workingHoursStartTextField.text = selectedTime
     } else {
     workingHoursEndTextField.text = selectedTime
     }
     }*/
    
    // MARK: IBActions
    
    /*  @IBAction func didChangeRequireMarinaEntry(_ sender: UISwitch) {
     hasMarinaAuthorizationSwitch.isEnabled = sender.isOn
     hasMarinaAuthorizationLabel.isEnabled = sender.isOn
     if !sender.isOn {
     hasMarinaAuthorizationSwitch.isOn = false
     }
     }*/
    
    @IBAction func didClickCreate(_ sender: UIButton) {
        view.endEditing(true)
        if let errorMessage = validate(order: order) {
            ProgressHUD.shared.show(.error, in: view, withMessage: errorMessage)
            ProgressHUD.shared.dismiss(afterDelay: 2.0)
        } else {
            UserDefaults.standard.set(selectedTimeFromIndex, forKey: "from_time")
            UserDefaults.standard.set(selectedTimeToIndex, forKey: "to_time")
            ProgressHUD.shared.show(.pleaseWait, in: view)
            ApiController.shared.createOrder(order) { result in
                switch result {
                case let .success(order):
                    ProgressHUD.shared.show(.success, in: self.view, withMessage: "message_trip_created".localized)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0) {
                        self.ordersViewController?.add(order: order)
                        self.navigationController?.popViewController(animated: true)
                    }
                case let .failure(error):
                    ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0)
                }
            }
        }
    }
    
    // MARK: UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        //return 1
        if originRegionTextField.isFirstResponder || destinationRegionTextField.isFirstResponder || returnRegionTextField.isFirstResponder{
            return 2
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if component == 0 {
            
            if originRegionTextField.isFirstResponder || destinationRegionTextField.isFirstResponder || returnRegionTextField.isFirstResponder{
                return Location.sharedInstance.getCities(countryIndex: selectedCity).count
            }else if originCityTextField.isFirstResponder {
                return  Location.sharedInstance.getCities(countryIndex: selectedCity)[originIndex].areas.count
            }else if destinationCityTextField.isFirstResponder {
                return  Location.sharedInstance.getCities(countryIndex: selectedCity)[distinationIndex].areas.count
            }else if returnCityTextField.isFirstResponder {
                return  Location.sharedInstance.getCities(countryIndex: selectedCity)[returnIndex].areas.count
            }else if saveCostTextField.isFirstResponder {
                return Constants.saveCostTypes.count
            }else if orderTypeTextField.isFirstResponder {
                return Constants.orderTypes.count
            }else if workingHoursStartTextField.isFirstResponder {
                return times.count
            }else if workingHoursEndTextField.isFirstResponder {
                return times.count
            }else if paymentMethodsTextField.isFirstResponder {
                return Constants.paymentMethods.count
            } else {
                return Constants.tripTypes.count
            }
        } else {
            return Location.sharedInstance.getCountries().count
        }
        
    }
    
    // MARK: UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            
            if originRegionTextField.isFirstResponder || destinationRegionTextField.isFirstResponder || returnRegionTextField.isFirstResponder{
                return Location.sharedInstance.getCities(countryIndex: selectedCity)[row].getName(showAra: Constants.showArabic)
            } else if originCityTextField.isFirstResponder {
                return Location.sharedInstance.getCities(countryIndex: selectedCity)[originIndex].areas[row].getName(showAra: Constants.showArabic)
            } else if destinationCityTextField.isFirstResponder {
                return Location.sharedInstance.getCities(countryIndex: selectedCity)[distinationIndex].areas[row].getName(showAra: Constants.showArabic)
            } else if returnCityTextField.isFirstResponder {
                return Location.sharedInstance.getCities(countryIndex: selectedCity)[returnIndex].areas[row].getName(showAra: Constants.showArabic)
            } else if saveCostTextField.isFirstResponder {
                return Constants.saveCostTypes[row]
            } else if orderTypeTextField.isFirstResponder {
                return Constants.orderTypes[row]
            }  else if workingHoursStartTextField.isFirstResponder {
                return times[row]
            } else if workingHoursEndTextField.isFirstResponder {
                return times[row]
            } else if paymentMethodsTextField.isFirstResponder {
                return Constants.paymentMethods[row]
            } else {
                return Constants.tripTypes[row]
            }
        }else {
            return Location.sharedInstance.getCountries()[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 1{
            selectedCity = row
            pickerView.reloadComponent(0)
        }else {
            if originRegionTextField.isFirstResponder {
                originRegionTextField.text = Location.sharedInstance.getCities(countryIndex: selectedCity)[row].getName(showAra: Constants.showArabic)
                originIndex = row
                topic = Location.sharedInstance.getCities(countryIndex: selectedCity)[row].id
            } else if originCityTextField.isFirstResponder {
                originCityTextField.text =  Location.sharedInstance.getCities(countryIndex: selectedCity)[originIndex].areas[row].getName(showAra: Constants.showArabic)
                originId = Location.sharedInstance.getCities(countryIndex: selectedCity)[originIndex].areas[row].id
            } else if destinationRegionTextField.isFirstResponder {
                destinationRegionTextField.text = Location.sharedInstance.getCities(countryIndex: selectedCity)[row].getName(showAra: Constants.showArabic)
                distinationIndex = row
            } else if destinationCityTextField.isFirstResponder {
                destinationCityTextField.text =  Location.sharedInstance.getCities(countryIndex: selectedCity)[distinationIndex].areas[row].getName(showAra: Constants.showArabic)
                distinationId = Location.sharedInstance.getCities(countryIndex: selectedCity)[distinationIndex].areas[row].id
            }else if returnRegionTextField.isFirstResponder {
                returnIndex = row
                returnRegionTextField.text = Location.sharedInstance.getCities(countryIndex: selectedCity)[row].getName(showAra: Constants.showArabic)
            } else if returnCityTextField.isFirstResponder {
                returnCityTextField.text =  Location.sharedInstance.getCities(countryIndex: selectedCity)[returnIndex].areas[row].getName(showAra: Constants.showArabic)
                returnId = Location.sharedInstance.getCities(countryIndex: selectedCity)[returnIndex].areas[row].id
            } else if saveCostTextField.isFirstResponder {
                saveCostTextField.text = Constants.saveCostTypes[row]
                saveCostValue = row
            }  else if orderTypeTextField.isFirstResponder {
                orderTypeTextField.text = Constants.orderTypes[row]
                orderType = row
            } else if workingHoursStartTextField.isFirstResponder {
                workingHoursStartTextField.text = times[row]
                selectedTimeFromIndex = row
            }  else if workingHoursEndTextField.isFirstResponder {
                workingHoursEndTextField.text = times[row]
                selectedTimeToIndex = row
            } else if paymentMethodsTextField.isFirstResponder {
                paymentMethodsTextField.text = Constants.paymentMethods[row]
                paymentMethod = row
            } else {
                roundTrip = row == 1
                checkTripType()
                tripTypeTextField.text = Constants.tripTypes[row]
            }
        }
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        switch textField.returnKeyType {
        case .next:
            if let nextResponder = view.viewWithTag(nextTag) {
                nextResponder.becomeFirstResponder()
            }
        default:
            return true
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.inputView is UIPickerView {
            pickerView.reloadAllComponents()
            for i in 0 ..< pickerView(pickerView, numberOfRowsInComponent: 0) {
                if pickerView(pickerView, titleForRow: i, forComponent: 0) == textField.text {
                    pickerView.selectRow(i, inComponent: 0, animated: true)
                    break
                }
            }
        } else if textField == originAddressTextField {
            presentPlacePicker(type: .Origin)
        }else if textField == destinationAddressTextField {
            presentPlacePicker(type: .Destination)
        }else if textField == returnAddressTextField {
            presentPlacePicker(type: .Return)
        }
    }
    
    // MARK: GMSPlacePickerViewControllerDelegate
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        UIApplication.shared.statusBarStyle = .lightContent
        viewController.dismiss(animated: true)
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        UIApplication.shared.statusBarStyle = .lightContent
        viewController.dismiss(animated: true)
        var formattedAddress: String = place.formattedAddress ?? ""
        if formattedAddress.isEmpty {
            formattedAddress = "\(place.coordinate.latitude), \(place.coordinate.longitude)"
        }
        switch placePickerPresented! {
        case .Origin:
            originAddressTextField.text = formattedAddress
            originCoordinates = Coordinates(clCoordinates: place.coordinate)
            break
        case .Destination:
            destinationAddressTextField.text = formattedAddress
            destinationCoordinates = Coordinates(clCoordinates: place.coordinate)
            break
        case .Return:
            returnAddressTextField.text = formattedAddress
            returnCoordinates = Coordinates(clCoordinates: place.coordinate)
            break
        }
        
        
    }
    
    func checkTripType()  {
        if roundTrip {
            returnRegionView.isHidden = false
            returnCityView.isHidden = false
            returnAddressTextField.isHidden = false
        }else {
            returnRegionView.isHidden = true
            returnCityView.isHidden = true
            returnAddressTextField.isHidden = true
        }
    }
    

    
}
