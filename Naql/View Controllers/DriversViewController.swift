//
//  DriversViewController.swift
//  Naql
//
//  Created by Admin on 3/7/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import UIKit

class DriversViewController: UIViewController ,UITableViewDelegate, UITableViewDataSource,PassDataProtocol {
    
    
    
    
    
    @IBOutlet weak var driverTable: UITableView!
    private var refreshControl = UIRefreshControl()
    
    var list :[Driver] = []
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        driverTable.dataSource = self
        driverTable.delegate = self
        driverTable.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refresh()
        
    }
    
    
    @objc private func refresh() {
        refreshControl.beginRefreshing()
        ApiController.shared.fetchDrivers { result in
            self.list.removeAll()
            switch result {
            case let .success(drivers):
                self.list = drivers
                self.driverTable.reloadData()
            case let .failure(error):
                ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                ProgressHUD.shared.dismiss(afterDelay: 2.0)
            }
            
            self.refreshControl.endRefreshing()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "driver", for: indexPath) as! MiniDriverTableViewCell
        cell.driver = list[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != list.count {
            performSegue(withIdentifier: "driver_details", sender: list[indexPath.row])
        }
        selectedIndex = indexPath.row
        //tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "driver_details" {
            let viewController = segue.destination as! AddEditDriverViewController
            viewController.driver = sender as? Driver ?? Driver()
            viewController.passDataPro = self
        }else if segue.identifier == "add_driver" {
            let viewController = segue.destination as! AddEditDriverViewController
            viewController.passDataPro = self
        }
    }
    
    func passData<T>(genericClass: T) {
        if genericClass is Driver {
            let driver = genericClass as! Driver
            if selectedIndex >= 0 {
                list[selectedIndex] = driver
                let indexPath = IndexPath(item: selectedIndex, section: 0)
                driverTable.reloadRows(at: [indexPath], with: .fade)
            }else if selectedIndex == -1 {
                list.append(driver)
                driverTable.beginUpdates()
                driverTable.insertRows(at: [IndexPath.init(row: 0, section: 0)], with: .fade)
                driverTable.endUpdates()
            }
        }else {
            let indexPath = IndexPath(item: selectedIndex, section: 0)
            list.remove(at: selectedIndex)
            driverTable.deleteRows(at: [indexPath], with: .fade)
        }
    }
    @IBAction func addDriver(_ sender: Any) {
        selectedIndex = -1
        performSegue(withIdentifier: "add_driver", sender:self)
    }
    
}
