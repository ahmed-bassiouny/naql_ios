//
//  LanuchScreenViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/19/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

class LaunchScreenViewController: UIViewController {
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Thread.sleep(forTimeInterval: 0.5)
        getCities()
        
    }
    
    func nextScreen() {
        if UserController.shared.isUserSignedIn {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            guard let rootViewController = mainStoryboard.instantiateInitialViewController() else {
                fatalError("Couldn't instantiate the initial view controller!")
            }
            presentAsKey(rootViewController, completion: nil)
        } else {
            performSegue(withIdentifier: "LoginSegue", sender: self)
        }
    }
    func getCities() {
        ApiController.shared.fetchCountries() { result in
            switch result{
            case let .failure(error):
                // show error
                print(error)
                exit(0)
                break
            case let .success(countries):
                Location.countries = countries
                self.nextScreen()
                break
            }
        }
    }
}
