//
//  OrderDetailsViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/21/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

class OrderDetailsViewController: UIViewController {
    @IBOutlet weak var orderStatusLabel: UILabel!
    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var orderNumberNameLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var originCityLabel: UILabel!
    @IBOutlet weak var originAddressLabel: UILabel!
    @IBOutlet weak var destinationCityLabel: UILabel!
    @IBOutlet weak var destinationAddressLabel: UILabel!
    @IBOutlet weak var orderTypeLabel: UILabel!
    @IBOutlet weak var tripTypeLabel: UILabel!
    @IBOutlet weak var numberOfShipmentsLabel: UILabel!
    @IBOutlet weak var numberOfShipmentRequiredLabel: UILabel!
    @IBOutlet weak var orderWeightLabel: UILabel!
    @IBOutlet weak var proposedPriceLabel: UILabel!
    @IBOutlet weak var orderDateLabel: UILabel!
    @IBOutlet weak var workingHoursStartLabel: UILabel!
    @IBOutlet weak var workingHoursEndLabel: UILabel!
    @IBOutlet weak var holidaysLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sendDriversButton: UIButton!
    
    @IBOutlet weak var showDetailsLabel: UIButton!
    @IBOutlet weak var returnCityLabel: UILabel!
    
    @IBOutlet weak var returnAddressLabel: UILabel!
    
    @IBOutlet weak var savingCostLabel: UILabel!
    @IBOutlet weak var paymentMethodsLabel: UILabel!
    
    @IBOutlet weak var numberOfDriverLabel: UILabel!
    
    @IBOutlet weak var numberOfDriversValueLabel: UILabel!
    private let dateFormatter = DateFormatter(withFormat: "dd/MM/yyyy", locale: Locale.current.identifier)
    
    var order: Order!
    
    var ordersViewController: OrdersViewController?
    
    var hideShowMoreDetailsInfoForDriverCompany = true
    override func viewDidLoad() {
        super.viewDidLoad()
        fillInfo()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MoreDetailsSegue" {
            let viewController = segue.destination as! MoreOrderDetailsViewController
            viewController.order = order
        } else if segue.identifier == "AddDriverSegue" {
            let viewController = segue.destination as! AddDriverViewController
            viewController.orderDetailsViewController = self
            viewController.order = order
        }
    }
    
    // MARK: Private functions
    
    private func fillInfo() {
        if getNumberOfMaxDriver(order.drivers.count,order.numberOfDrivers) == order.numberOfShipmentsAvailable {
            orderStatusLabel.text = "label_inactive".localized
            orderStatusLabel.backgroundColor = .red
        } else {
            orderStatusLabel.text = ("label_active").localized
            orderStatusLabel.backgroundColor = .greenDark
        }
        let currentUserType = UserController.shared.currentUser?.type ?? .factory
        sendDriversButton.isHidden = currentUserType == .factory
        cancelButton.isHidden = currentUserType == .driver
        companyNameLabel.text = order.companyName
        originCityLabel.text = order.fromCity
        originAddressLabel.text = order.fromAddress
        destinationCityLabel.text = order.toCity
        destinationAddressLabel.text = order.toAddress
        orderTypeLabel.text = Constants.orderTypes[order.orderType]
        tripTypeLabel.text = Constants.tripTypes[order.tripType]
        numberOfShipmentRequiredLabel.text = String(format: "%i", order.numberOfShipmentsAvailable)
        numberOfShipmentsLabel.text = String(format: "%i", order.numberOfShipmentsAvailable - order.numberOfDrivers)
        orderWeightLabel.text = order.loadInTon
        proposedPriceLabel.text = order.price //String(format: "%.2f", )
        orderDateLabel.text = dateFormatter.string(from: order.createdAt)
        workingHoursStartLabel.text = order.fromTime > 0 ? Constants.times[order.fromTime] : ""
        workingHoursEndLabel.text = order.toTime > 0 ? Constants.times[order.toTime] : ""
        holidaysLabel.text = order.holiday
        returnCityLabel.text = order.returnCity
        returnAddressLabel.text = order.returnAddress
        savingCostLabel.text = Constants.saveCostTypes[order.saveCost]
        numberOfDriversValueLabel.text = String(format: "%i", order.numberOfDrivers)
        paymentMethodsLabel.text = Constants.paymentMethods[order.paymentMethod]
        
        let currentUser = UserController.shared.currentUser!
        if currentUser.type == .driver {
            // driver company
            // cant cancel any trip
            cancelButton.isHidden = true
            // show details if trip have drivers
            showDetailsLabel.isHidden = order.companies.count == 0 && order.drivers.count == 0
            
            // send drivers if status not cancel and driver > 0
            if order.status == .canceled || order.drivers.count != 0{
                sendDriversButton.isHidden = true
            }
            numberOfDriverLabel.isHidden = false
            numberOfDriversValueLabel.isHidden = false
            orderNumberNameLabel.text = "bill_number".localized
            orderNumberLabel.text = "\(currentUser.id)\(order.id)"
            
        }else {
            // company
            // can cancel trip if status not cancel
            cancelButton.isHidden = order.status == .canceled
            // cant send driver
            sendDriversButton.isHidden = true
            // always show details
            showDetailsLabel.isHidden = false
            numberOfDriverLabel.isHidden = true
            numberOfDriversValueLabel.isHidden = true
            orderNumberNameLabel.text = "order_number".localized
            orderNumberLabel.text = String(format: "%i", order.id)
        }
                
    }
    
    // MARK: IBActions
    
    @IBAction func didClickCancel(_ sender: UIButton) {
        let uiAlert = UIAlertController(title: "cancel_trip".localized, message: "are_you_sure".localized, preferredStyle: UIAlertController.Style.alert)
        
        uiAlert.addAction(UIAlertAction(title: "yes".localized, style: .default, handler: { action in
            ApiController.shared.cancelOrder(self.order.id) { result in
                switch result {
                case let .success(message):
                    ProgressHUD.shared.show(.success, in: self.view, withMessage: message)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0) {
                        self.navigationController?.popViewController(animated: true)
                    }
                    self.ordersViewController?.cancelOrder(withId: self.order.id)
                case let .failure(message):
                    ProgressHUD.shared.show(.success, in: self.view, withMessage: message)
                    ProgressHUD.shared.dismiss(afterDelay: 2.0)
                }
            }
        }))
        
        uiAlert.addAction(UIAlertAction(title: "no".localized, style: .cancel, handler: { action in
        }))
        self.present(uiAlert, animated: true, completion: nil)
    }
    
    @IBAction func didClickAddDriver(_ sender: UIButton) {
        let numberOfAllowedDrivers = order.numberOfShipmentsAvailable - order.numberOfDrivers
        if numberOfAllowedDrivers <= 0 {
            AlertUtils.showAlert(parent: self, message: "error_not_allowed_to_add_driver".localized)
        } else {
            performSegue(withIdentifier: "AddDriverSegue", sender: self)
        }
    }
    
    func addedDriversToTrip(numberOfDrivers: Int) {
        order.numberOfDrivers = order.numberOfDrivers + numberOfDrivers
        numberOfDriversValueLabel.text = String(format: "%i", order.numberOfDrivers)
        
    }
    
    func getNumberOfMaxDriver(_ first:Int,_ second:Int) -> Int{
        return max(first, second)
    }
    
    
    @IBAction func printAction(_ sender: Any) {
        print()
    }
    
    func print(){
        var direction = "left";
        var lay = "ltr";
        if (Locale.current.languageCode == "ar") {
            direction = "right";
            lay = "rtl";
        }
        
        let body = "<html dir=\(lay)>" +
            "<head>" +
            "<style>" +
            "table {" +
            "  border-collapse: collapse;" +
            "  width: 100%;" +
            "}" +
            "" +
            "td, th {" +
            "  border: 1px solid #dddddd;" +
            "  text-align: \(direction);" +
            "  padding: 8px;" +
            "}" +
            "" +
            "</style>" +
            "</head>" +
            "<body>" +
            "<table>" +
            "  <tr>" +
            "    <td>\("trip_id".localized)</td>" +
            "    <th>\(order.id)</th>" +
            "  </tr>" +
            "  <tr>" +
            "    <td>\("company_name".localized)</td>" +
            "    <td>\(order.companyName)</td>" + // 1
            "  </tr>" +
            "  <tr>" +
            "    <td>\("from_city".localized)</td>" +
            "    <td>\(order.fromCity)</td>" + //2
            "  </tr>" +
            "  <tr>" +
            "    <td>\("from_address".localized)</td>" +
            "    <td>\(order.fromAddress)</td>" + //3
            "  </tr>" +
            "  <tr>" +
            "    <td>\("to_city".localized)</td>" +
            "    <td>\(order.toCity)</td>" + //4
            "  </tr>" +
            "  <tr>" +
            "    <td>\("to_address".localized)</td>" +
            "    <td>\(order.toAddress)</td>" + //5
            "  </tr>" +
            "  <tr>" +
            "    <td>\("trip_type".localized)</td>" +
            "    <td>\(Constants.orderTypes[order.orderType])</td>" + //6
            "  </tr>" +
            "  <tr>" +
            "    <td>\("order_type".localized)</td>" +
            "    <td>\(Constants.tripTypes[order.tripType])</td>" + //7
            "  </tr>" +
            "  <tr>" +
            "    <td>\("shipments".localized)</td>" +
            "    <td>\(order.numberOfShipmentsAvailable)</td>" + //8
            "  </tr>" +
            "  <tr>" +
            "    <td>\("avaliableshipments".localized)</td>" +
            "    <td>\(String(format: "%i", order.numberOfShipmentsAvailable - order.numberOfDrivers))</td>" + //9
            "  </tr>" +
            "  <tr>" +
            "    <td>\("loadintop".localized)</td>" +
            "    <td>\(order.loadInTon)</td>" + //10
            "  </tr>" +
            "  <tr>" +
            "    <td>\("price".localized)</td>" +
            "    <td>\(order.price)</td>" + //11
            "  </tr>" +
            "</table>" +
            "" +
            "</body>" +
        "</html>"
        
        var web = UIWebView()
        web.loadHTMLString(body, baseURL: nil)
        
        let pi = UIPrintInfo.printInfo()
        pi.outputType = .general
        //pi.jobName = urlCheck.absoluteString
        pi.orientation = .portrait
        pi.duplex = .longEdge
        
        let printController = UIPrintInteractionController.shared
        printController.printInfo = pi
        printController.printFormatter = web.viewPrintFormatter()
        printController.present(animated: true, completionHandler: nil)
    }
}
