//
//  NewCarViewController.swift
//  Naql
//
//  Created by Admin on 1/23/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import UIKit

class NewCarViewController: UIViewController {
    
    
    
    @IBOutlet weak var carTypeField: UITextField!
    @IBOutlet weak var modelField: UITextField!
    @IBOutlet weak var plateNumberField: UITextField!
    @IBOutlet weak var colorField: UITextField!
   // @IBOutlet weak var formNumberField: UITextField!
   // @IBOutlet weak var formDateField: UITextField!
    @IBOutlet weak var activeSwitch: UISwitch!
    
    @IBOutlet weak var deleteButton: UIButton!
    
    var car = Car()
//    private lazy var datePicker: UIDatePicker = {
//        let datePicker = UIDatePicker()
//        datePicker.datePickerMode = .date
//        datePicker.minimumDate = Date()
//        datePicker.addTarget(self, action: #selector(didSelectDate), for: .valueChanged)
//        return datePicker
//    }()
    //private let dateFormatter = DateFormatter(withFormat: "dd-MM-yyyy", locale: Locale.current.identifier)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if car.id > 0 {
            setData()
            deleteButton.isHidden = false
        }else {
            deleteButton.isHidden = true
            
        }
        
        //formDateField.inputView = datePicker
    }
    
    
//    @objc private func didSelectDate(_ sender: Any) {
//       // let selectedDate = dateFormatter.string(from: datePicker.date)
//        //formDateField.text = selectedDate
//    }
    
    
    @IBAction func save(_ sender: Any) {
        if validInputs() {
            saveCar()
        }
    }
    
    @IBAction func deleteCar(_ sender: Any) {
        let uiAlert = UIAlertController(title: "delete".localized, message: "are_you_sure".localized, preferredStyle: UIAlertController.Style.alert)
        
        uiAlert.addAction(UIAlertAction(title: "yes".localized, style: .default, handler: { action in
            self.deleteCar()
        }))
        
        uiAlert.addAction(UIAlertAction(title: "no".localized, style: .cancel, handler: { action in
        }))
        self.present(uiAlert, animated: true, completion: nil)
    }
    
    func setData() {
        carTypeField.text = car.carType
        modelField.text = car.model
        plateNumberField.text = car.plateNumber
        colorField.text = car.color
        //formNumberField.text = car.formNumber
        //formDateField.text = car.formEndDate
        activeSwitch.isOn = !car.deleted
        
    }
    
    func validInputs() -> Bool {
        if carTypeField.text?.isEmpty ?? true || modelField.text?.isEmpty ?? true ||
            plateNumberField.text?.isEmpty ?? true || colorField.text?.isEmpty ?? true {
            //formNumberField.text?.isEmpty ?? true || formDateField.text?.isEmpty ?? true {
            ProgressHUD.shared.show(.error, in: view, withMessage: "all_field_required".localized)
            ProgressHUD.shared.dismiss(afterDelay: 1.0)
            return false
        }
        return true
    }
    
    func saveCar()  {
        car.carType = carTypeField.text ?? ""
        car.model = modelField.text ?? ""
        car.plateNumber = plateNumberField.text ?? ""
        car.color = colorField.text ?? ""
        //car.formNumber = formNumberField.text ?? ""
        //car.formEndDate = formDateField.text ?? ""
        
        view.endEditing(true)
        ProgressHUD.shared.show(.pleaseWait, in: view)
        ApiController.shared.addOrEditCar(car) { result in
            switch result {
            case let .success(message):
                ProgressHUD.shared.dismiss(afterDelay: 1.0) {
                    self.navigationController?.popViewController(animated: true)
                }
            case let .failure(error):
                ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                ProgressHUD.shared.dismiss(afterDelay: 1.0)
            }
        }
        
    }
    
    func deleteCar()  {
        
        car.deleted = true
        view.endEditing(true)
        ProgressHUD.shared.show(.pleaseWait, in: view)
        ApiController.shared.addOrEditCar(car) { result in
            switch result {
            case let .success(message):
                ProgressHUD.shared.dismiss(afterDelay: 1.0) {
                    self.navigationController?.popViewController(animated: true)
                }
            case let .failure(error):
                ProgressHUD.shared.show(.error, in: self.view, withMessage: error)
                ProgressHUD.shared.dismiss(afterDelay: 1.0)
            }
        }
        
    }
    
}
