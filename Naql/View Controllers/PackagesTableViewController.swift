//
//  PackagesTableViewController.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/21/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import Foundation
import UIKit

class PackagesTableViewController: UITableViewController {
    private var packages: [Package] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        tableView.refreshControl = refreshControl
        refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refresh()
    }

    // MARK: Private functions

    @objc private func refresh() {
        refreshControl?.beginRefreshing()
        ApiController.shared.fetchPackages { result in
            self.packages.removeAll()
            switch result {
            case let .success(packages):
                self.packages.append(contentsOf: packages)
            case let .failure(error):
                self.display(message: error)
            }
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        }
    }

    private func display(message: String) {
        AlertUtils.showAlert(parent: self, message: message)
    }

    // MARK: UITableViewDataSource

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        if indexPath.row == packages.count {
            let identifier = "FooterTableCell"
            cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        } else {
            let identifier = "PackageTableCell"
            cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
            (cell as! PackageTableCell).package = packages[indexPath.row]
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return packages.count + 1
    }

    // MARK: UITableViewDelegate

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
