//
//  ApiController.swift
//  Events
//
//  Created by Mostafa Saleh on 9/13/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import Foundation
import ObjectMapper

class ApiController {
    static let shared = ApiController()
    
    private let BASE_URL = "http://naaqll.com/naaqll.com/public"
    
    private init() {
    }
    
    func signIn(email: String, password: String, completion: @escaping (ApiResult<User>) -> Void) {
        let url = "\(BASE_URL)/api/login"
        let params = ["email": email, "password": password]
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<UserResponse>) in
                switch response.result {
                case let .success(result):
                    if result.status, let user = result.data {
                        completion(.success(user))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func register(user: User, withPassword password: String, completion: @escaping (ApiResult<User>) -> Void) {
        let url = "\(BASE_URL)/api/registration"
        var params = user.toJSON()
        params["password"] = password
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<UserResponse>) in
                switch response.result {
                case let .success(result):
                    if result.status, let user = result.data {
                        completion(.success(user))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func registerDevice(notificationToken: String, completion: @escaping (ApiResult<Void?>) -> Void) {
        //        let currentUser = UserManager.shared.currentUser!
        //        let url = "\(BASE_URL)/api/notification_token"
        //        let params = ["event_id": 1, "user_id": currentUser.id, "notification_token": notificationToken] as [String: Any]
        //        Alamofire.request(url, method: .post, parameters: params)
        //            .responseObject { (response: DataResponse<BaseResponse>) in
        //                switch response.result {
        //                case let .success(result):
        //                    if result.status {
        //                        completion(.success(nil))
        //                    } else {
        //                        completion(.failure(result.message))
        //                    }
        //                case let .failure(error):
        //                    completion(.failure(error.localizedDescription))
        //                }
        //        }
        completion(.success(nil))
    }
    
    func sendPasswordResetEmail(to email: String, completion: @escaping (ApiResult<()?>) -> Void) {
        let url = "\(BASE_URL)/api/forget_password"
        let params = ["email": email]
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<BaseResponse>) in
                switch response.result {
                case let .success(result):
                    if result.status {
                        completion(.success(nil))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func editProfile(for user: User, completion: @escaping (ApiResult<User>) -> Void) {
        let url = "\(BASE_URL)/api/profile/edit"
        var params = user.toJSON()
        params["user_id"] = user.id
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<UserResponse>) in
                switch response.result {
                case let .success(result):
                    if result.status, let user = result.data {
                        completion(.success(user))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func changePassword(to password: String, oldPassword: String, completion: @escaping (ApiResult<Void?>) -> Void) {
        let currentUser = UserController.shared.currentUser!
        let url = "\(BASE_URL)/api/password/edit"
        let params = ["user_id": currentUser.id, "password": password, "old_password": oldPassword] as [String: Any]
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<BaseResponse>) in
                switch response.result {
                case let .success(result):
                    if result.status {
                        completion(.success(nil))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func fetchPage(_ page: Page, completion: @escaping (ApiResult<String>) -> Void) {
        let currentUser = UserController.shared.currentUser!
        let url = "\(BASE_URL)/api/page"
        let params = ["user_id": currentUser.id, "name": page.stringValue] as [String: Any]
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<PageResponse>) in
                switch response.result {
                case let .success(result):
                    if result.status, let body = result.data {
                        completion(.success(body))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func fetchComplaints(completion: @escaping (ApiResult<[Complaint]>) -> Void) {
        let currentUser = UserController.shared.currentUser!
        let url = "\(BASE_URL)/api/compliant/list"
        let params = ["user_id": currentUser.id] as [String: Any]
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<ListResponse<Complaint>>) in
                switch response.result {
                case let .success(result):
                    if result.status {
                        completion(.success(result.data))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func addComplaint(_ complaint: Complaint, completion: @escaping (ApiResult<String>) -> Void) {
        let url = "\(BASE_URL)/api/compliant/create"
        let params = complaint.toJSON()
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<BaseResponse>) in
                switch response.result {
                case let .success(result):
                    if result.status {
                        completion(.success(result.message))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func fetchPackages(completion: @escaping (ApiResult<[Package]>) -> Void) {
        let currentUser = UserController.shared.currentUser!
        let url = "\(BASE_URL)/api/packages"
        let params = ["user_id": currentUser.id] as [String: Any]
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<ListResponse<Package>>) in
                switch response.result {
                case let .success(result):
                    if result.status {
                        completion(.success(result.data))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func fetchOrders(forDriver: Bool = false, past: Bool = false, page: Int, limit: Int, status: [OrderStatus], completion: @escaping (ApiResult<([Order], Int)>) -> Void) {
        let currentUser = UserController.shared.currentUser!
        let url: String
        var statusString = ""
        if forDriver {
            if past {
                url = "\(BASE_URL)/api/orders/with/company/with/drivers"
                statusString = "1";
            } else {
                url = "\(BASE_URL)/api/orders/still/open/orders"
                statusString = "1";
            }
        } else {
            statusString = "1";
            url = "\(BASE_URL)/api/orders"
        }
        let offset = limit * page
        //let statusString = status.map { TransfromString.ToOrderStatus().transformToJSON($0) ?? "" }.joined(separator: ",")
        let params = ["user_id": currentUser.id, "offset": offset, "limit": limit, "status": statusString, "lang":"language".localized] as [String: Any]
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<EndlessListResponse<Order>>) in
                switch response.result {
                case let .success(result):
                    if result.status, let orders = result.data?.list, let count = result.data?.count {
                        completion(.success((orders, count)))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
        
    }
    
    func createOrder(_ order: Order, completion: @escaping (ApiResult<Order>) -> Void) {
        let url = "\(BASE_URL)/api/orders/create"
        var params = order.toJSON()
        params["user_id"] = UserController.shared.currentUser?.id ?? 0
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<OrderResponse>) in
                switch response.result {
                case let .success(result):
                    if result.status, let order = result.data {
                        completion(.success(order))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func cancelOrder(_ orderId: Int, completion: @escaping (ApiResult<String>) -> Void) {
        let url = "\(BASE_URL)/api/orders/create/status"
        let userId = UserController.shared.currentUser?.id ?? 0
        let params = ["user_id": userId, "order_id": orderId, "status": 0] as [String: Any]
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<BaseResponse>) in
                switch response.result {
                case let .success(result):
                    if result.status {
                        completion(.success(result.message))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func addDrivers(to orderId: Int, drivers: [Driver], completion: @escaping (ApiResult<Void?>) -> Void) {
        let url = "\(BASE_URL)/api/drivers/create"
        let userId = UserController.shared.currentUser?.id ?? 0
        var string = "["
        
        for item in drivers {
            string += item.toJSONString() ?? ""
            string += ","
        }
        string.remove(at: string.index(before: string.endIndex))
        string += "]"
        var driverResult = [Dictionary<String,Any>]()
        let data = string.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
            {
                driverResult =  jsonArray // print(jsonArray) // use the json here
            }
        } catch let error as NSError {
            print(error)
        }
        
        
        let params = ["owner_id": userId, "order_id": orderId, "drivers": driverResult] as [String: Any]
        /* Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default,headers:nil)
         .responseJSON { response in
         print("JSON:\(response.result.value)")
         switch(response.result) {
         case .success(_):
         if let data = response.result.value{
         print(data)
         }
         
         case .failure(_):
         
         print("Error message:\(response.result.error)")
         break
         
         }
         }
         .responseString { response in
         print("request \(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue))")
         print("String:\(response.result.value)")
         switch(response.result) {
         case .success(_):
         if let data = response.result.value{
         print(data)
         }
         
         case .failure(_):
         print("Error message:\(response.result.error)")
         break
         }
         }*/
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default,headers:nil)
            .responseObject { (response: DataResponse<BaseResponse>) in
                switch response.result {
                    
                case let .success(result):
                    if result.status {
                        completion(.success(nil))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func fetchCountries(completion: @escaping (ApiResult<[Country]>) -> Void) {
        let url = "\(BASE_URL)/api/countries"
        Alamofire.request(url, method: .post)
            .responseObject { (response: DataResponse<ListResponse<Country>>) in
                switch response.result {
                case let .success(result):
                    if result.status {
                        completion(.success(result.data))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    
    
    func addOrEditCar(_ car: Car, completion: @escaping (ApiResult<String>) -> Void) {
        var url = ""
        if car.id > 0 {
            // edit case
            car.carId = car.id
            url = "\(BASE_URL)/api/cars/edit"
        } else {
            url = "\(BASE_URL)/api/cars/create"
        }
        let currentUser = UserController.shared.currentUser!
        car.userId = String(currentUser.id)
        let params = car.toJSON()
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<BaseResponse>) in
                switch response.result {
                case let .success(result):
                    if result.status {
                        completion(.success(result.message))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func fetchCars(completion: @escaping (ApiResult<[Car]>) -> Void) {
        let currentUser = UserController.shared.currentUser!
        let url = "\(BASE_URL)/api/cars"
        let params = ["user_id": currentUser.id,"is_deleted":"[0]"] as [String: Any]
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<ListResponse<Car>>) in
                switch response.result {
                case let .success(result):
                    if result.status {
                        completion(.success(result.data))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func fetchDrivers(completion: @escaping (ApiResult<[Driver]>) -> Void) {
        let currentUser = UserController.shared.currentUser!
        let url = "\(BASE_URL)/api/drivers/new/list"
        let params = ["owner_id": currentUser.id] as [String: Any]
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<ListResponse<Driver>>) in
                switch response.result {
                case let .success(result):
                    if result.status {
                        completion(.success(result.data))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func deleteDriver(driverId:Int,completion: @escaping (ApiResult<Void?>) -> Void) {
        let url = "\(BASE_URL)/api/delete/new/driver"
        let params = ["driver_id": driverId] as [String: Any]
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<BaseResponse>) in
                switch response.result {
                case let .success(result):
                    if result.status {
                        completion(.success(nil))
                    } else {
                        completion(.failure(result.message))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func addOrEditDriver(_ driver: Driver, completion: @escaping (ApiResult<Driver>) -> Void) {
        var url = ""
        if driver.id > 0 {
            // edit case
            url = "\(BASE_URL)/api/update/new/driver"
            driver.driverId = driver.id
        } else {
            url = "\(BASE_URL)/api/add/new/driver"
        }
        let currentUser = UserController.shared.currentUser!
        driver.ownerID = String(currentUser.id)
        let params = driver.toJSON()
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<DriverResponse>) in
                switch response.result {
                case let .success(result):
                    if result.status, let driver = result.data {
                        completion(.success(driver))
                    } else {
                        completion(.failure(result.message))
                    }
                    
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func json(from object:Any) -> String?  {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return ""
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    func fetchSearchForOrders(offset: Int, limit: Int,tripId:String,dateFrom:String,dateTo:String, completion: @escaping (ApiResult<[Order]>) -> Void) {
        
        let currentUser = UserController.shared.currentUser!
        let url: String
        let statusString = "1"
        if currentUser.type == .driver {
            url = "\(BASE_URL)/api/orders/search/send/drivers"
        } else {
            url = "\(BASE_URL)/api/orders/search"
        }
        //let statusString = status.map { TransfromString.ToOrderStatus().transformToJSON($0) ?? "" }.joined(separator: ",")
        let params = ["user_id": currentUser.id,"search_id":tripId,"from_date_created_at":dateFrom,"to_date_created_at":dateTo, "offset": offset, "limit": limit, "status": statusString, "lang":"language".localized] as [String: Any]
        Alamofire.request(url, method: .post, parameters: params)
            .responseObject { (response: DataResponse<EndlessListResponse<Order>>) in
                switch response.result {
                case let .success(result):
                    if result.status, let orders = result.data?.list{
                        completion(.success((orders)))
                    } else {
                        completion(.failure(result.message))
                    }
                    
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
        
    }
    
    
}
