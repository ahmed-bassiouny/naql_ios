//
//  ApiResult.swift
//  Events
//
//  Created by Mostafa Saleh on 9/13/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

enum ApiResult<Value> {
    case success(Value)
    case failure(String)
}
