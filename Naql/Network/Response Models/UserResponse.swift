//
//  LoginResponse.swift
//  Events
//
//  Created by Mostafa Saleh on 9/14/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class UserResponse: BaseResponse {
    var data: User?

    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}
