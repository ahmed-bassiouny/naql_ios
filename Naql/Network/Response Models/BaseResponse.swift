//
//  Response.swift
//  Events
//
//  Created by Mostafa Saleh on 9/13/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class BaseResponse: Mappable {
    var status: Bool = false
    private var messageAr: String = ""
    private var messageEn: String = ""
    var message: String {
        return Locale.current.languageCode == "ar" ? messageAr : messageEn
    }

    required init?(map: Map) {}

    open func mapping(map: Map) {
        status <- (map["status"], TransformInt.ToBool())
        messageAr <- map["message_ar"]
        messageEn <- map["messageـen"]
        if messageEn.isEmpty {
            messageEn <- map["message_en"]
        }
    }
}
