//
//  ComplaintListResponse.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/24/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class ListResponse<T: Mappable>: BaseResponse {
    var data: [T] = []

    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}
