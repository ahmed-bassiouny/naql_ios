//
// Created by Mostafa Saleh on 10/24/18.
// Copyright (c) 2018 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class PageResponse: BaseResponse {
    var data: String?

    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}
