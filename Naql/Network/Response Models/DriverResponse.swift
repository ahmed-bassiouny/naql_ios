//
//  DriverResponse.swift
//  Naql
//
//  Created by Admin on 3/7/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class DriverResponse: BaseResponse {
    var data: Driver?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}
