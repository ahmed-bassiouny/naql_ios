//
//  OrderResponse.swift
//  Naql
//
//  Created by Mostafa Saleh on 12/6/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class OrderResponse: BaseResponse {
    var data: Order?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}
