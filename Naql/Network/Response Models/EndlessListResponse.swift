//
//  EndlessListResponse.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/21/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class EndlessListResponse<T: Mappable>: BaseResponse {
    var data: Data?

    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }

    class Data: Mappable {
        var count: Int = 0
        var list: [T] = []

        required init?(map: Map) {}

        func mapping(map: Map) {
            count <- map["count"]
            list <- map["list"]
        }
    }
}
