//
//  UIViewController.swift
//  Events
//
//  Created by Mostafa Saleh on 9/4/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

extension UIViewController {
    func presentAsKey(_ viewControllerToPresent: UIViewController, animationDuration duration: TimeInterval = 0.3, animation: UIView.AnimationOptions = .transitionFlipFromLeft, completion: (() -> Void)? = nil) {
        if let window = UIApplication.shared.keyWindow {
            UIView.transition(with: window, duration: duration, options: animation, animations: {
                window.rootViewController = viewControllerToPresent
                window.makeKeyAndVisible()
            }, completion: { _ in
                completion?()
            })
        }
    }
}
