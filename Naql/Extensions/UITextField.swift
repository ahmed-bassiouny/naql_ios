//
//  UITextField.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/17/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

extension UITextField {
    @IBInspectable var placeholderColor: UIColor? {
        get {
            let attributes = attributedPlaceholder?.attributes(at: 0, effectiveRange: nil) ?? [:]
            return attributes[NSAttributedString.Key.foregroundColor] as? UIColor ?? .placeholderGray
        }

        set {
            let color: UIColor = newValue ?? .placeholderGray
            attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: color])
        }
    }
    
    @IBInspectable var rightImage: UIImage? {
        get {
            let rightImageView = rightView as? UIImageView
            return rightImageView?.image
        }
        
        set {
            if let newImage = newValue {
                rightViewMode = .always
                if let rightImageView = rightView as? UIImageView {
                    rightImageView.image = newImage
                } else {
                    rightView = UIImageView(image: newImage)
                    rightView!.frame = CGRect(x: 0.0, y: 0.0, width: newImage.size.width + 10.0, height: newImage.size.height)
                    rightView!.contentMode = .center
                }
            } else {
                rightView = nil
                rightViewMode = .never
            }
        }
    }
}
