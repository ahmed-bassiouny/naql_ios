//
//  NSMutableAttributedString.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/30/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import Foundation

extension NSMutableAttributedString {
    open func addAttribute(_ name: NSAttributedString.Key, value: Any) {
        addAttribute(name, value: value, range: NSRange(location: 0, length: length))
    }
}
