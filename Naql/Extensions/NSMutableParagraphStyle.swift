//
//  NSMutableParagraphStyle.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/30/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

extension NSMutableParagraphStyle {
    convenience init(lineSpacing: CGFloat) {
        self.init()
        self.lineSpacing = lineSpacing
    }
}
