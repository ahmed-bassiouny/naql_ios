//
//  UINavigationBar.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/16/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

extension UINavigationBar {
    @IBInspectable var backgroundImage: UIImage? {
        set {
            setBackgroundImage(newValue, for: .default)
        }
        get {
            return backgroundImage(for: .default)
        }
    }
}
