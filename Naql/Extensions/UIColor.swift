//
//  UIColor.swift
//  Events
//
//  Created by Mostafa Saleh on 9/5/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

extension UIColor {
    static var colorPrimary: UIColor {
        return UIColor(red: 57, green: 129, blue: 202)
    }

    static var colorAccent: UIColor {
        return UIColor(red: 255, green: 0, blue: 0)
    }

    static var transparent: UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0)
    }

    static var placeholderGray: UIColor {
        return UIColor(red: 138, green: 138, blue: 138)
    }

    static var orangeLight: UIColor {
        return UIColor(red: 239, green: 159, blue: 72)
    }

    static var orangeDark: UIColor {
        return UIColor(red: 243, green: 117, blue: 69)
    }

    static var greenDark: UIColor {
        return UIColor(red: 34, green: 139, blue: 34)
    }

    convenience init(red: Int, green: Int, blue: Int, alpha: Float = 1) {
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: CGFloat(alpha))
    }
}
