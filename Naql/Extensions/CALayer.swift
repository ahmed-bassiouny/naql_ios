//
//  CALayer.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/16/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import Foundation
import UIKit

extension CALayer {
    var image: UIImage? {
        var image: UIImage? = nil
        UIGraphicsBeginImageContext(bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return image
    }
}
