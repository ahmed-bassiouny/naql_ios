//
//  BoolTransform.swift
//  Events
//
//  Created by Mostafa Saleh on 9/13/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class TransformInt {
    class ToBool: TransformType {
        typealias Object = Bool
        typealias JSON = Int

        func transformFromJSON(_ value: Any?) -> Bool? {
            if let value = value as? String {
                return value == "1" ? true : false
            } else if let value = value as? Int {
                return value == 1 ? true : false
            } else if let value = value as? Bool {
                return value
            }
            return false
        }

        func transformToJSON(_ value: Bool?) -> Int? {
            return value == true ? 1 : 0
        }
    }
}
