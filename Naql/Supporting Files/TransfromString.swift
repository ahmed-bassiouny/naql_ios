//
//  StringTransform.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/21/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import ObjectMapper

class TransfromString {
    class ToDouble: TransformType {
        typealias Object = Double
        typealias JSON = String

        func transformFromJSON(_ value: Any?) -> Double? {
            if let value = value as? String {
                return Double(value) ?? 0
            }
            return 0
        }

        func transformToJSON(_ value: Double?) -> String? {
            if let value = value {
                return "\(value)"
            }
            return nil
        }
    }

    class ToInt: TransformType {
        typealias Object = Int
        typealias JSON = String

        func transformFromJSON(_ value: Any?) -> Int? {
            if let value = value as? String {
                return Int(value) ?? 0
            }
            return 0
        }

        func transformToJSON(_ value: Int?) -> String? {
            if let value = value {
                return "\(value)"
            }
            return nil
        }
    }
    
 

    class ToDate: TransformType {
        public typealias Object = Date
        public typealias JSON = String

        public init() {}

        open func transformFromJSON(_ value: Any?) -> Date? {
            if let timeStr = value as? String {
                return DateFormatter(withFormat: "yyyy-MM-dd HH:mm:ss", locale: "en_US_POSIX").date(from: timeStr)
            }
            return nil
        }

        open func transformToJSON(_ value: Date?) -> String? {
            if let date = value {
                return DateFormatter(withFormat: "yyyy-MM-dd HH:mm:ss", locale: "en_US_POSIX").string(from: date)
            }
            return nil
        }
    }

    class ToUserType: TransformType {
        typealias Object = UserType
        typealias JSON = String

        func transformFromJSON(_ value: Any?) -> UserType? {
            if let value = value as? Int {
                return value == 1 ? .factory : .driver
            } else if let value = value as? String {
                return value == "1" ? .factory : .driver
            }
            return .driver
        }

        func transformToJSON(_ value: UserType?) -> String? {
            return value == .driver ? "2" : "1"
        }
    }

    class ToOrderStatus: TransformType {
        typealias Object = OrderStatus
        typealias JSON = String

        func transformFromJSON(_ value: Any?) -> OrderStatus? {
            if let value = value as? Int {
                return value == 0 ? .canceled : .running
            } else if let value = value as? String {
                return value == "0" ? .canceled : .running
            }
            return nil
        }

        func transformToJSON(_ value: OrderStatus?) -> String? {
            return value == .canceled ? "0" : "1"
        }
    }
}
