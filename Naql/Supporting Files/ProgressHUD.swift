//
//  ProgressUtils.swift
//  Naql
//
//  Created by Mostafa Saleh on 10/28/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import JGProgressHUD

class ProgressHUD {
    static let shared = ProgressHUD()
    private let progressHUD = JGProgressHUD()

    func show(_ type: ProgressType, in view: UIView, withMessage message: String = "") {
        switch type {
        case .pleaseWait:
            progressHUD.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        case .error:
            progressHUD.indicatorView = JGProgressHUDErrorIndicatorView()
        case .success:
            progressHUD.indicatorView = JGProgressHUDSuccessIndicatorView()
        }
        var message = message
        if message.isEmpty {
            if type == .pleaseWait {
                message = "message_please_wait".localized
            } else if type == .error {
                message = "error_unknown".localized
            }
        }
        progressHUD.textLabel.text = message
        progressHUD.show(in: view)
    }

    func dismiss(afterDelay delay: TimeInterval = 0.0, completion: (() -> Void)? = nil) {
        progressHUD.dismiss(afterDelay: delay)
        if let completion = completion {
            DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: completion)
        }
    }

    enum ProgressType {
        case pleaseWait
        case error
        case success
    }
}
