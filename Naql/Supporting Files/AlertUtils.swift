//
//  AlertUtils.swift
//  Events
//
//  Created by Mostafa Saleh on 9/9/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import UIKit

class AlertUtils {
    static func showAlert(parent: UIViewController, message: String, title: String? = nil, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: handler)
        alert.addAction(okAction)
        parent.present(alert, animated: true, completion: nil)
    }
    
    static func showConfirmationAlert(parent: UIViewController, message: String, title: String? = nil, handler: @escaping ((UIAlertAction) -> Void)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default, handler: handler)
        alert.addAction(yesAction)
        let noAction = UIAlertAction(title: "No", style: .cancel, handler: handler)
        alert.addAction(noAction)
        parent.present(alert, animated: true, completion: nil)
    }
}
