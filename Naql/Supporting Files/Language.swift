//
//  Language.swift
//  Naql
//
//  Created by Admin on 2/22/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import UIKit


private let appleLanguagesKey = "AppleLanguages"


enum Language: String {
    
    case english = "en"
    case arabic = "ar"
    
    var semantic: UISemanticContentAttribute {
        switch self {
        case .english:
            return .forceLeftToRight
        case .arabic:
            return .forceRightToLeft
        }
    }
    
    
    static var language: Language {
        get {
            if let languageCode = UserDefaults.standard.string(forKey: appleLanguagesKey),
                let language = Language(rawValue: languageCode) {
                return language
            } else {
                let preferredLanguage = NSLocale.preferredLanguages[0] as String
                let index = preferredLanguage.index(
                    preferredLanguage.startIndex,
                    offsetBy: 2
                )
                guard let localization = Language(
                    rawValue: String(preferredLanguage[..<index])
                    ) else {
                        return Language.english
                }
                
                return localization
            }
        }
        set {
            guard language != newValue else {
                return
            }
            
            //change language in the app
            //the language will be changed after restart
            UserDefaults.standard.set([newValue.rawValue], forKey: appleLanguagesKey)
            UserDefaults.standard.synchronize()
            
            //Changes semantic to all views
            //this hack needs in case of languages with different semantics: leftToRight(en/uk) & rightToLeft(ar)
            UIView.appearance().semanticContentAttribute = newValue.semantic
            
            //initialize the app from scratch
            //show initial view controller
            //so it seems like the is restarted
            //NOTE: do not localize storboards
            //After the app restart all labels/images will be set
            //see extension String below
           /* UIApplication.shared.windows[0].rootViewController = UIStoryboard(
                name: "Login",
                bundle: nil
                ).instantiateInitialViewController()*/
        }
    }
}

extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    var localizedImage: UIImage? {
        return localizedImage()
            ?? localizedImage(type: ".png")
            ?? localizedImage(type: ".jpg")
            ?? localizedImage(type: ".jpeg")
            ?? UIImage(named: self)
    }
    
    private func localizedImage(type: String = "") -> UIImage? {
        guard let imagePath = Bundle.localizedBundle.path(forResource: self, ofType: type) else {
            return nil
        }
        return UIImage(contentsOfFile: imagePath)
    }
}

extension Bundle {
    //Here magic happens
    //when you localize resources: for instance Localizable.strings, images
    //it creates different bundles
    //we take appropriate bundle according to language
    static var localizedBundle: Bundle {
        let languageCode = Language.language.rawValue
        guard let path = Bundle.main.path(forResource: languageCode, ofType: "lproj") else {
            return Bundle.main
        }
        return Bundle(path: path)!
    }
}

class LanguageUtils {
    
   static func changeLanguageToAR() {
        if Language.language == .arabic {
            return
        }
        Language.language = .arabic
        UserDefaults.standard.set(Constants.ar, forKey: Constants.language)
        UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            exit(EXIT_SUCCESS)
        })
    }
    
    static func changeLanguageToEN() {
        if Language.language == .english {
            return
        }
        Language.language = .english
        UserDefaults.standard.set(Constants.en, forKey: Constants.language)
        UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            exit(EXIT_SUCCESS)
        })
    }
    
    static func changeLanguage(vc:UIViewController){
        let alertController = UIAlertController(title: nil, message: "change_language".localized, preferredStyle: .actionSheet)
        
        let action1 = UIAlertAction(title: "اللغة العربية", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            changeLanguageToAR()
            
            
        })
        
        let action2 = UIAlertAction(title: "English", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            changeLanguageToEN()
            
        })
        
        
        let maybeAction = UIAlertAction(title: "cancel".localized, style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
        })
        
        //        let image1 = UIImage(named: "qr-code")
        //        let image2 = UIImage(named: "phone_number")
        //
        //
        //        action1.setValue(image1, forKey: "image")
        //        action1.setValue(Utils.hexStringToUIColor(hex: "#2fa1ab"), forKey: "titleTextColor")
        //        action2.setValue(image2, forKey: "image")
        //        action2.setValue(Utils.hexStringToUIColor(hex: "#2fa1ab"), forKey: "titleTextColor")
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(maybeAction)
        
        vc.present(alertController, animated: true, completion: nil)
    }
}
