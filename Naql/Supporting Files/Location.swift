//
//  Location.swift
//  Naql
//
//  Created by Admin on 1/11/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import Foundation

class Location {
    
    static let sharedInstance = Location()
    
    static var countries :[Country] = []
    
    private var cities:[[String]] = [[""],[""]]
    
    private var Countries:[String] = ["ksa".localized,"aue".localized]
    
    private static var  region_cities :Dictionary<String,[String]> = [:]
    
    private init(){
    }
    
    
    func getRegions() -> [String] {
        return [""]
    }
    
    func getCountries() -> [String] {
        return Countries
    }
    
    func getCities(countryIndex:Int) -> [City] {
        return Location.countries[countryIndex].cities
    }
    
    func getRegion_Cities() -> Dictionary<String,[String]>  {
        return Location.region_cities //Dictionary<String,[String]>
    }
    
    private func createArabicRegions_Cities(){
       /*
        var c1 = [""]
        c1.append("الرياض")
        c1.append("الدرعية")
        c1.append("الخرج")
        c1.append("الدوادمي")
        c1.append("المجمعة")
        c1.append("القويعية")
        c1.append("الأفلاج")
        c1.append("وادي الدواسر")
        c1.append("الزلفي")
        c1.append("شقراء")
        c1.append("حوطة بني تميم")
        c1.append("عفيف")
        c1.append("الغاط")
        c1.append("السليل")
        c1.append("ضرما")
        c1.append("المزاحمية")
        c1.append("رماح")
        c1.append("ثادق")
        c1.append("حريملاء")
        c1.append("الحريق")
        c1.append("مرات")
        c1.append("الرين")
        region_cities["الرياض"] = c1
        
        
        var c2 = [""]
        c2.append("مكة المكرمة")
        c2.append("جدة")
        c2.append("الطائف")
        c2.append("القنفذة")
        c2.append("الليث")
        c2.append("رابغ")
        c2.append("خليص")
        c2.append("الخرمة")
        c2.append("رنية")
        c2.append("تربة")
        c2.append("الجموم")
        c2.append("الكامل")
        c2.append("المويه")
        c2.append("ميسان")
        c2.append("أضم")
        c2.append("العرضيات")
        c2.append("بحرة")
        region_cities["مكة المكرمة"] = c2
        
        var c3 = [""]
        c3.append("المدينة المنورة")
        c3.append("ينبع")
        c3.append("العلا")
        c3.append("مهد الذهب")
        c3.append("الحناكية")
        c3.append("بدر")
        c3.append("خيبر")
        c3.append("العيص")
        c3.append("وادي الفرع")
        region_cities["المدينة المنورة"] = c3
        
        var c4 = [""]
        c4.append("القصيم")
        c4.append("بريدة")
        c4.append("عنيزة")
        c4.append("الرس")
        c4.append("المذنب")
        c4.append("البكيرية")
        c4.append("البدائع")
        c4.append("الأسياح")
        c4.append("النبهانية")
        c4.append("الشماسية")
        c4.append("عيون الجواء")
        c4.append("رياض الخبراء")
        c4.append("عقلة الصقور")
        c4.append("ضرية")
        region_cities["القصيم"] = c4
        
        var c5 = [""]
        c5.append("المنطقة الشرقية")
        c5.append("الدمام")
        c5.append("الأحساء")
        c5.append("حفر الباطن")
        c5.append("الجبيل")
        c5.append("القطيف")
        c5.append("الخبر")
        c5.append("الخفجي")
        c5.append("رأس تنورة")
        c5.append("بقيق")
        c5.append("النعيرية")
        c5.append("قرية العليا")
        c5.append("العديد")
        region_cities["المنطقة الشرقية"] = c5
        
        var c6 = [""]
        c6.append("عسير")
        c6.append("أبها")
        c6.append("خميس مشيط")
        c6.append("بيشة")
        c6.append("النماص")
        c6.append("محايل عسير")
        c6.append("ظهران الجنوب")
        c6.append("تثليث")
        c6.append("سراة عبيدة")
        c6.append("رجال ألمع")
        c6.append("بلقرن")
        c6.append("أحد رفيدة")
        c6.append("المجاردة")
        c6.append("البرك")
        c6.append("بارق")
        c6.append("تنومة")
        c6.append("طريب")
        c6.append("الحرجة")
        region_cities["عسير"] = c6
        
        var c7 = [""]
        c7.append("تبوك")
        c7.append("الوجه")
        c7.append("ضبا")
        c7.append("تيماء")
        c7.append("أملج")
        c7.append("حقل")
        c7.append("البدع")
        region_cities["تبوك"] = c7
        
        var c8 = [""]
        c8.append("حائل")
        c8.append("بقعاء")
        c8.append("الغزالة")
        c8.append("الشنان")
        c8.append("الحائط")
        c8.append("السليمي")
        c8.append("الشملي")
        c8.append("موقق")
        region_cities["حائل"] = c8
        
        var c9 = [""]
        c9.append("الحدود الشمالية")
        c9.append("عرعر")
        c9.append("رفحاء")
        c9.append("طريف")
        c9.append("العويقيلة")
        region_cities["الحدود الشمالية"] = c9
        
        
        var c10 = [""]
        c10.append("جازان")
        c10.append("صبيا")
        c10.append("أبو عريش")
        c10.append("صامطة")
        c10.append("بيش")
        c10.append("الدرب")
        c10.append("الحرث")
        c10.append("ضمد")
        c10.append("الريث")
        c10.append("جزر فرسان")
        c10.append("الدائر")
        c10.append("العارضة")
        c10.append("أحد المسارحة")
        c10.append("العيدابي")
        c10.append("فيفاء")
        c10.append("الطوال")
        c10.append("هروب")
        region_cities["جازان" ] = c10
        
        var c11 = [""]
        c11.append("نجران")
        c11.append("شرورة")
        c11.append("حبونا")
        c11.append("بدر الجنوب")
        c11.append("يدمه")
        c11.append("ثار")
        c11.append("خباش")
        c11.append("الخرخير")
        region_cities["نجران"] = c11
        
        var c12 = [""]
        c12.append("الباحة")
        c12.append("بلجرشي")
        c12.append("المندق")
        c12.append("المخواة")
        c12.append("قلوة")
        c12.append("العقيق")
        c12.append("القرى")
        c12.append("غامد الزناد")
        c12.append("الحجرة")
        c12.append("بني حسن")
        region_cities["الباحة"] = c12
        
        var c13 = [""]
        c13.append("الجوف")
        c13.append("سكاكا")
        c13.append("دومة الجندل")
        c13.append("طبرجل")
        region_cities["الجوف"] = c13
       */
    }
}

