//
//  PassDataProtocol.swift
//  Naql
//
//  Created by Admin on 3/7/19.
//  Copyright © 2019 Mostafa Saleh. All rights reserved.
//

import Foundation

protocol PassDataProtocol {
    
    func passData<T>(genericClass: T?)
   
}
