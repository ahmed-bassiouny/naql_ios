//
import ObjectMapper
//  LoginManager.swift
//  Events
//
//  Created by Mostafa Saleh on 9/10/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//
import UIKit

class UserController {
    static var shared = UserController()

    private let KEY_USER_SIGNED_IN = "UserSignedIn"
    private let KEY_USER = "User"
    private let KEY_NOTIFICATION = "Notification"
    
    var currentUser: User? {
        get {
            if let userData = UserDefaults.standard.dictionary(forKey: KEY_USER) {
                return Mapper<User>().map(JSON: userData)
            }
            return nil
        }
        set {
            if let userJSON = newValue?.toJSON() {
                UserDefaults.standard.set(userJSON, forKey: KEY_USER)
            } else {
                UserDefaults.standard.removeObject(forKey: KEY_USER)
            }
        }
    }

    private(set) var isUserSignedIn: Bool {
        get {
            return UserDefaults.standard.bool(forKey: KEY_USER_SIGNED_IN)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_USER_SIGNED_IN)
        }
    }

    private init() {
    }

    func signIn(email: String, password: String, completion: @escaping (ApiResult<Void?>) -> Void) {
        ApiController.shared.signIn(email: email, password: password) { result in
            switch result {
            case let .success(user):
//                user.notificationToken = Messaging.messaging().fcmToken ?? "1"
                self.currentUser = user
                self.registerDevice(for: user, completion: completion)
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }

    func registerDevice(for user: User, completion: @escaping (ApiResult<Void?>) -> Void) {
        ApiController.shared.registerDevice(notificationToken: user.notificationToken) { result in
            switch result {
            case .success:
                self.isUserSignedIn = true
                completion(.success(nil))
            case let .failure(error):
                self.currentUser = nil
                completion(.failure(error))
            }
        }
    }

    func signOut(from sender: UIViewController, withError error: Bool = false) {
        isUserSignedIn = false
        currentUser = nil
        let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let loginViewController = loginStoryboard.instantiateInitialViewController()!
        sender.presentAsKey(loginViewController) {
            if error {
                AlertUtils.showAlert(parent: loginViewController, message: "error_user_nil".localized, title: "title_error".localized)
            }
        }
    }
    
    var notification:[Int] {
        get {
            return UserDefaults.standard.array(forKey: KEY_NOTIFICATION)  as? [Int] ?? [Int]()
        }
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_NOTIFICATION)
        }
    }
}
