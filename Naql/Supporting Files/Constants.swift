//
//  Constans.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/20/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import Foundation

class Constants {
   /* static let locations = ["مكة المكرمة", "المدينة المنورة", "القصيم", "الشرقية", "عسير", "تبوك", "حائل", "الحدود الشمالية", "جازان", "الباحة", "الجوف"]

    static let subLocation = ["":[],"تبوك":["s","sss"],"جازان":["aa","aaaa"]] as [String : [String]]*/
    static let orderTypes = ["t1".localized,"t2".localized,"t3".localized,"t4".localized,"t5".localized,"t6".localized] // ["سطحه", "كونتينر", "جوانب", "لوبد", "ثلاجه", "ستائر"]

    static let tripTypes = ["one_way".localized,"round_trip".localized]
    
    static let saveCostTypes = ["save_cost_1".localized,"save_cost_2".localized,"save_cost_3".localized,"save_cost_4".localized]
    static let showArabic = true

    static let times = ["avaiable_24".localized,"12:00 am","12:30 am","01:00 am","01:30 am","02:00 am","02:30 am","03:00 am","03:30 am"
        ,"04:00 am","04:30 am","05:00 am","05:30 am","06:00 am","06:30 am","07:00 am","07:30 am","08:00 am","08:30 am","09:00 am","09:30 am","10:00 am","10:30 am","11:00 am","11:30 am","12:00 pm","12:30 pm","01:00 pm","01:30 pm","02:00 pm","02:30 pm","03:00 pm","03:30 pm"
        ,"04:00 pm","04:30 pm","05:00 pm","05:30 pm","06:00 pm","06:30 pm","07:00 pm","07:30 pm","08:00 pm","08:30 pm","09:00 pm","09:30 pm","10:00 pm","10:30 pm","11:00 pm","11:30 pm"]
    
    static let language = "language"
    static let ar = "ar"
    static let en = "en"
    static let paymentMethods = ["cash".localized,"installment_debt".localized]
}
