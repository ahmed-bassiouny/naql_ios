//
//  GradientButton.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/16/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import Foundation
import UIKit

class GradientView: UIView {
    @IBInspectable var startColor: UIColor? {
        didSet {
            updateBackgroundGradient()
        }
    }
    
    @IBInspectable var centerColor: UIColor? {
        didSet {
            updateBackgroundGradient()
        }
    }
    
    @IBInspectable var endColor: UIColor? {
        didSet {
            updateBackgroundGradient()
        }
    }
    
    private func updateBackgroundGradient() {
        let gradient = CAGradientLayer()
        var updatedBounds = bounds
        updatedBounds.size.height += frame.origin.y
        updatedBounds.size.width = UIScreen.main.bounds.size.width
        gradient.frame = updatedBounds
        let colors = [startColor, centerColor, endColor]
        gradient.colors =
            colors
                .filter { color in color != nil }
                .map { color in return color!.cgColor }
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 0, y: 1)
        layer.insertSublayer(gradient, at: 0)
    }
}
