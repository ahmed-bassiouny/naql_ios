//
// Created by Mostafa Saleh on 10/24/18.
// Copyright (c) 2018 Mostafa Saleh. All rights reserved.
//

import WebKit

class WKWebView_IBWrapper: WKWebView {
    required convenience init?(coder: NSCoder) {
        self.init(frame: .zero, configuration: WKWebView_IBWrapper.buildWebViewConfig())
        translatesAutoresizingMaskIntoConstraints = false
    }

    private static func buildWebViewConfig() -> WKWebViewConfiguration {
        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let wkUController = WKUserContentController()
        wkUController.addUserScript(userScript)
        let config = WKWebViewConfiguration()
        config.userContentController = wkUController
        return config
    }
}
