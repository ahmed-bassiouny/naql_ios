//
//  PaddedMDCTextField.swift
//  Naql
//
//  Created by Mostafa Saleh on 11/23/18.
//  Copyright © 2018 Mostafa Saleh. All rights reserved.
//

import MaterialComponents
import UIKit

class PaddedMDCTextField: MDCTextField {
    @IBInspectable var endPadding: CGFloat = 0 {
        didSet {
            layoutIfNeeded()
        }
    }

    open override func textRect(forBounds bounds: CGRect) -> CGRect {
        let padding: UIEdgeInsets
        if UIView.userInterfaceLayoutDirection(for: semanticContentAttribute) == .rightToLeft {
            padding = UIEdgeInsets(top: 0, left: endPadding, bottom: 0, right: 0)
        } else {
            padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: endPadding)
        }
        return super.textRect(forBounds: bounds).inset(by: padding)
    }

    open override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        let padding: UIEdgeInsets
        if UIView.userInterfaceLayoutDirection(for: semanticContentAttribute) == .rightToLeft {
            padding = UIEdgeInsets(top: 0, left: endPadding, bottom: 0, right: 0)
        } else {
            padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: endPadding)
        }
        return super.placeholderRect(forBounds: bounds).inset(by: padding)
    }

    open override func editingRect(forBounds bounds: CGRect) -> CGRect {
        let padding: UIEdgeInsets
        if UIView.userInterfaceLayoutDirection(for: semanticContentAttribute) == .rightToLeft {
            padding = UIEdgeInsets(top: 0, left: endPadding, bottom: 0, right: 0)
        } else {
            padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: endPadding)
        }
        return super.editingRect(forBounds: bounds).inset(by: padding)
    }
}
